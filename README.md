# dune-composites - README

dune-composites is a Dune PDELab module which implements 3D anisotropic linear elasticity equations in parallel

Class documentation for the module can be found at:

https://www.dune-project.org/doxygen/dune-composites/release-2.5/index.html

https://www.dune-project.org/doxygen/dune-composites/release-2.6/index.html

## Installation of dune-composites module (assuming Dune is already installed) ##

clone into dune-composites folder, then call

```
./dune-common/bin/dunecontrol --builddir=[path - to - dune - installation]/build/release all
```

In the file [path - to - dune - installation]/build/release/dune-composites/src
you can make and run your file for example


```
make problem01a && ./problem01a
```



## File List ##
```
 dune	
    composites	
        Driver	
            FEM	
                Serendipity	
                    serendipity.hh                      #Serendipity Lagrange finite elements for cubes
                    serendipityfem.hh                   #Wrap up element from local functions
                    serendipitylocalbasis.hh	        #Serendipity basis functions of order k on the reference cube
                    serendipitylocalcoefficients.hh     #Attaches a shape function to an entity
                    serendipitylocalinterpolation.hh    #Interpolation
            localOperator	
                linearelasticity.hh                     #Local operator for linear elasticity
            Solvers	
                hypre	
                    globalDOFNumbering.hh               #global index ordering
                    hypreinterface.hh                   #interface to hypre solver
                    hypreinterface_sequential.hh        #simpler interface for sequential runs
                    parallelhelper.hh                   #helper function for parallel interface
                geneo.hh                                #wrapper for geneo solver
                zembasis.hh                             #zero energy modes for elasticity
                solver_info.hh                          #class containing solver settings
                solvers.hh                              #header containing solvers
            heatStaticDriver.hh                         #Static driver including a thermal loading
            linearStaticDriver.hh                       #Basic static driver
        MathFunctions	
            mathfunctions.hh                            #header containing math functions
            transpose.hh                                #transpose of a field matrix
        PostProcessing	
            FailureCriteria
                camanhoCriterion.hh                     #Camanho failure criterion
                failureCriteria.hh                      #header containing failure criteria
            composite_properties_lop.hh                 #local operator to calculate material properties for plotting
            computeStresses.hh                          #wrapper for stress calculation
            getStress.hh                                #local operator for stress calculation
            plot_properties.hh                          #wrapper for material property plotting
        Setup	
            RandomFields	
                defectGenerator.hh                      #Constructor for COEFF class
                general.hh                              #helper functions: mean, variance
                integrator.hh                           #integrator for defect
                KLFunctions.hh                          #KL modes
                readData.hh                             #Read data files
            baseStructuredGridModel.hh                  #structuredGridModel provides a base class for 3D structure grid models, ie all default functions for definition of a user defined model.
            Cijkl.hh                                    #Elasticity Tensor on each element and contains all required functions
            definePG.hh                                 #Define physical groups
            dirichletBC.hh                              #Define Scalar Dirichlet Boundary Conditions
            GridTransformation.hh                       #transformations of points x in the base grid (generated using LayerCake()) to the final structural geometry
            Layer.hh                                    #the Model::LayerCake() class can be extended to also define layers properties of each layer, including thickness, material type, or orientation.
            Material.hh                                 #Class which stores information about a material e.g. AS4/8552
            parallelPartition.hh                        #Partition yaspgrid for parallelism
            Region.hh                                   #The Region class stores information about the different regions of the geometry
```

## Sample output -- Sequential ##
```
./Example01a 
```

```
*NumRegions
1
Number of Regions = 1
maxPly = 23
periodic = 000
=== Building Geometry
Elements in x direction 
Number of elements per processor: 3500
Number of nodes per processor: 4536
Grid transformation complete
Grid view set up
Piecewise quadratic serendipity elements
=== matrix setup (max) 0.35446 s
=== matrix assembly (max) 1.50212 s
=== residual assembly (max) 1.1347 s
=== solving (reduction: 1e-06) 
UMFPACK V5.7.1 (Oct 10, 2014): OK

[UMFPack Decomposition]
Wallclock Time taken: 20.6132 (CPU Time: 20.6132)
Flops taken: 2.08993e+11
Peak Memory Usage: 9.71335e+08 bytes
Condition number estimate: 405877
Numbers of non-zeroes in decomposition: L: 5.19058e+07 U: 5.19038e+07

UMFPACK V5.7.1 (Oct 10, 2014): OK

[UMFPack Solve]
Wallclock Time: 0.420715 (CPU Time: 0.420715)
Flops Taken: 4.85993e+08
Iterative Refinement steps taken: 0
Error Estimate: 1.52811e-15 resp. 0
21.332 s
*** EXAMPLE01a COMPLETE ***
Maximum Vertical Displacement in Example01a = 1.23992mm
```

## Sample Output ##
```
mpirun -np 4 ./Example01b
```
```
*NumRegions
1
Number of Regions = 1
maxPly = 23
periodic = 000
=== Building Geometry
Elements in x direction 
Number of elements per processor: 1050
Number of nodes per processor: 1512
Grid transformation complete
Grid view set up
Piecewise quadratic serendipity elements
Starting solve with Geneo Preconditioner
Eigenvalue threshhold: 0.00909091
Process 3 picked 6 eigenvectors
Process 0 picked 1 eigenvectors
Process 2 picked 6 eigenvectors
Process 1 picked 6 eigenvectors
Matrix setup
Global basis size B=19
Matrix setup finished: M=0.148806
Geneo setup time 40.1707
=== CGSolver
   46      1.11274e-05
=== rate=0.769381, T=6.02243, TIT=0.130922, IT=46
Min eigv estimate: 0.0249189
Max eigv estimate: 2.9999
Condition estimate: 120.386
Solver: CG
Preconditioner: GenEO
Subdomain Solver: UMFPack
=================
Solver Converged: 1Solution of my Problem = 0.000166984
```
