# This CMake file from the quinoa project is under the following license:

# Copyright (c) 2016-2018, Los Alamos National Security, LLC
# All rights reserved.
#
# Copyright 2016-2018. Los Alamos National Security, LLC. This software was
# produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos
# National Laboratory (LANL), which is operated by Los Alamos National Security,
# LLC for the U.S. Department of Energy. The U.S. Government has rights to use,
# reproduce, and distribute this software. NEITHER THE GOVERNMENT NOR LOS ALAMOS
# NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY
# LIABILITY FOR THE USE OF THIS SOFTWARE. If software is modified to produce
# derivative works, such modified software should be clearly marked, so as not to
# confuse it with the version available from LANL.
#
# Additionally, redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation and/or
# other materials provided with the distribution.
#
# 3. Neither the name of Los Alamos National Security, LLC, Los Alamos National
# Laboratory, LANL, the U.S. Government, nor the names of its contributors may be
# used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
# OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
# OF SUCH DAMAGE.

################################################################################
#
# \file      cmake/FindHypre.cmake
# \copyright 2012-2015, J. Bakosi, 2016-2018, Los Alamos National Security, LLC.
# \brief     Find the Hypre library from LLNL
#
################################################################################

# Find the Hypre library from LLNL
#
#  HYPRE_FOUND - System has Hypre
#  HYPRE_INCLUDE_DIRS - The Hypre include directory
#  HYPRE_LIBRARIES - The libraries needed to use Hypre
#
#  Set HYPRE_ROOT before calling find_package to a path to add an additional
#  search path, e.g.,
#
#  Usage:
#
#  set(HYPRE_ROOT "/path/to/custom/hypre") # prefer over system
#  find_package(Hypre)
#  if(HYPRE_FOUND)
#    target_link_libraries (TARGET ${HYPRE_LIBRARIES})
#  endif()

function(_HYPRE_GET_VERSION _OUT_ver _version_hdr)
  file(STRINGS ${_version_hdr} _contents REGEX "#define HYPRE_RELEASE_VERSION[ \t]+")
  if(_contents)
    string(REGEX REPLACE "\"" "" _cont "${_contents}")
    string(REGEX REPLACE ".*#define HYPRE_RELEASE_VERSION[ \t]+([0-9.]+).*" "\\1" ${_OUT_ver} "${_cont}")
    if(NOT ${${_OUT_ver}} MATCHES "[0-9]+")
        message(FATAL_ERROR "Version parsing failed for HYPRE_RELEASE_VERSION in ${_version_hdr}!")
    endif()
    set(${_OUT_ver} ${${_OUT_ver}} PARENT_SCOPE)
 elseif(EXISTS ${_version_hdr})
    message(FATAL_ERROR "No HYPRE_RELEASE_VERSION in ${_version_hdr}")
 else()
    message(FATAL_ERROR "Include file ${_version_hdr} does not exist")
  endif()
endfunction()

# If already in cache, be silent
if(HYPRE_INCLUDE_DIRS AND HYPRE_LIBRARIES)
  set (HYPRE_FIND_QUIETLY TRUE)
endif()

find_path(HYPRE_INCLUDE_DIR NAMES HYPRE.h
                            PATH_SUFFIXES hypre
                            HINTS ${HYPRE_ROOT}/include)

if(HYPRE_INCLUDE_DIR)
  _HYPRE_GET_VERSION(HYPRE_VERSION ${HYPRE_INCLUDE_DIR}/HYPRE_config.h)
else()
  set(HYPRE_VERSION 0.0.0)
endif()

find_library(HYPRE_LIBRARY NAMES HYPRE_core HYPRE HINTS ${HYPRE_ROOT}/lib)

set(HYPRE_INCLUDE_DIRS ${HYPRE_INCLUDE_DIR})
set(HYPRE_LIBRARIES ${HYPRE_LIBRARY})

# Handle the QUIETLY and REQUIRED arguments and set HYPRE_FOUND to TRUE if
# all listed variables are TRUE.
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Hypre REQUIRED_VARS HYPRE_LIBRARIES HYPRE_INCLUDE_DIRS VERSION_VAR HYPRE_VERSION)

MARK_AS_ADVANCED(HYPRE_INCLUDE_DIRS HYPRE_LIBRARIES)

if(HYPRE_FOUND)
    set (HAVE_HYPRE 1)
    dune_register_package_flags(
    LIBRARIES "${HYPRE_LIBRARIES}"
    INCLUDE_DIRS "${HYPRE_INCLUDE_DIRS}")
else()
    set (HAVE_HYPRE 0)
endif()
