# module providing convenience methods for compiling binaries with Hypre support
#
# Provides the following functions:
#
# add_dune_hypre_flags(target1 target2...)
#
# adds Hypre flags to the targets for compilation and linking
function(add_dune_hypre_flags _targets)
  if(HYPRE_FOUND)
    foreach(_target ${_targets})
      target_link_libraries(${_target} ${HYPRE_LIBRARIES})
      set_property(TARGET ${_target} APPEND PROPERTY INCLUDE_DIRECTORIES ${HYPRE_INCLUDE_DIRS} )
    endforeach(_target ${_targets})
  endif(HYPRE_FOUND)
endfunction(add_dune_hypre_flags)
