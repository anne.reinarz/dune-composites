#include <dune/composites/composites.hh>

#include "Example01/Example01a.hh"

//===============================================================
// Main program with grid initialiseSolve
//===============================================================

int main(int argc, char** argv){

	const int DIM = 3;

	// Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    // Read ini file
    /*Dune::ParameterTreeParser parser;
    parser.readINITree("config_problem1a.ini",config);
    //read in further options from command line
    parser.readOptions(argc,argv,config);
    parser.readINITree("config.ini",configuration);*/

    Example01a<DIM> myModel(helper);

    myModel.apply();

    if(helper.rank() == 0)
    {
        std::cout << "*** EXAMPLE01a COMPLETE ***" << std::endl;
        std::cout << "Maximum Vertical Displacement in Example01a = " << myModel.Q << "mm" << std::endl;
    }
}
