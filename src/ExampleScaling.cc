#include <dune/composites/composites.hh>
#include "ExampleScaling/ExampleScaling.hh"

//===============================================================
// Main program with grid initialiseSolve
//===============================================================

int main(int argc, char** argv){

    //Read ini file
    Dune::ParameterTreeParser parser;
    //parser.readINITree("config.ini",config);
    //read in further options from command line
    parser.readOptions(argc,argv,config);

	const int DIM = 3;

    // Initialize MPI
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    // Set up and run model
    ExampleScaling<DIM> myModel(helper);
    myModel.apply();

    if(helper.rank() == 0) std::cout << "Maximum deflection = " << myModel.Q << std::endl;
}
