#include <dune/composites/composites.hh>

#include "Example04/Example04.hh"

//===============================================================
// Main program with grid initialiseSolve
//===============================================================

int main(int argc, char** argv){

  const int DIM = 3;

  // Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    Example04<DIM> myModel(helper);

    int N = 200;

    std::vector<double> Q(N);

    for (int i = 0; i < N; i++)
    {
      if(helper.rank() == 0)
        std::cout << "=== Run " << i+1 << std::endl;

      myModel.generateWrinkle();

      myModel.apply();

      Q[i] = myModel.Q;

    }


}
