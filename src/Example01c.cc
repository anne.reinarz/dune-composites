#include <dune/composites/composites.hh>

#include "Example01/Example01c.hh"

//===============================================================
// Main program with grid initialiseSolve
//===============================================================

int main(int argc, char** argv){

    const int DIM = 3;

    // Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    /* // Read ini file
       Dune::ParameterTreeParser parser;
       parser.readINITree("config_Example01a.ini",config);
    //read in further options from command line
    parser.readOptions(argc,argv,config);
    parser.readINITree("config.ini",configuration);*/

    Example01b<DIM> myModel(helper);

    myModel.apply();

    if(helper.rank() == 0) {

        std::cout << "Solver: " << myModel.solverParameters.solver << std::endl;
        std::cout << "Preconditioner: " << myModel.solverParameters.preconditioner << std::endl;
        std::cout << "Subdomain Solver: " << myModel.solverParameters.subdomainSolver << std::endl;

        std::cout << "=================" << std::endl;
        std::cout << "Solver Converged: " << myModel.solverParameters.results.converged;
        //std::cout << "Solver Iterations: " <<
        //std::cout << "Solver Iterations: " <<
        //std::cout << "Solver Iterations: " <<

        std::cout << "Solution of my Problem = " << myModel.Q << std::endl;
    }
}
