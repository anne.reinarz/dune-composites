#include <dune/composites/composites.hh>

#include "Example01UG/Example01UG.hh"

//===============================================================
// Main program with grid initialiseSolve
//===============================================================

int main(int argc, char** argv){

	const int DIM = 3;

	// Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
#if HAVE_UG
    Example01UG<DIM> myModel(helper);
    myModel.apply();
#else
    std::cout << "Need dune-uggrid module to run this test." << std::endl;
#endif
}
