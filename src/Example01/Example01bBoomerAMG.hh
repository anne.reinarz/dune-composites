// -*- tab-width: 2; indent-tabs-mode: nil -*-
// vi: set et ts=2 sw=2 sts=2:
#include<dune/composites/Setup/baseGridModel.hh>
#include <dune/composites/Driver/linearStaticDriver.hh>
#include <dune/composites/PostProcessing/FailureCriteria/camanhoCriterion.hh>

template<int DIM>
class Example01bBoomerAMG : public Dune::Composites::baseGridModel< Dune::YaspGrid<3,Dune::TensorProductCoordinates<double,3>>>{

public:

  double Q;

  int overlap;
  bool verbosity;

  Dune::Composites::linearStaticDriver<Example01bBoomerAMG<DIM>> myDriver;

  Example01bBoomerAMG(Dune::MPIHelper& helper_) : baseGridModel(helper_), myDriver(helper_)
  {

    overlap = 1;
    verbosity = true;

    solverParameters.solver = "Hypre";
    solverParameters.preconditioner = "BoomerAMG";
    solverParameters.KrylovTol = 1e-5;

    q = 1e-2; //pressure of 50 Atmospheres in MPa

    refineBaseGrid = 0;
  };


  inline void apply()
  {
    myDriver.apply(*this);
  }

  inline std::array<int, 3> GridPartition()
  {
    if(helper.size() not_eq 32)
      throw std::invalid_argument("You can only run this example with 32 processors!");
    // Function returns default partition
    std::array<int, 3> Partitioning;
    Partitioning[0] = 8;
    Partitioning[1] = 4;
    Partitioning[2] = 1;
    return Partitioning;
  }


  inline bool isDirichlet(FieldVec & x, const int dof)
  {
    //! Default isDirichlet function, returning homogeneous Dirichlet boundary conditioners
    return (x[0] < 1e-6);
  }

  inline void evaluateWeight(FieldVec& f, int id) const
  {
    f = 0.0;
  }

  inline void evaluateNeumann(const FieldVec &x,FieldVec& h,const  FieldVec& normal) const
  {
    h = 0.0; // initialise to zero
    double T = 2.98; // Thickness
    if (x[2] > T - 1e-6){ // If element on top face of laminate, apply neumann boundary condition
      h[2] += q;
    }
  }

  //! \fn A member taking which constructs the base layered laminate in untransformed coordinates
  inline void LayerCake()
  {
    std::string example01_Geometry = "stackingSequences/example1b.csv";
    LayerCakeFromFile(example01_Geometry);
    GeometryBuilder();
  }

  template<class GO, class U, class GFS, class C,class MBE, class GV>
  inline void postprocess(const GO& go, U& u, const GFS& gfs, const C& cg, const GV gv, MBE& mbe)
  {
    const std::vector<double> param = {61., 97., 94.}; //material allowables in MPa
    double F_max = 0.0;
    for (unsigned int i = 0; i < sig.size(); i++){ // For the stress in each element
      double F = Dune::Composites::Camanho(sig[i], elemIndx2PG[i], param);
      if(F_max < F){ F_max = F;}
    }
    MPI_Allreduce(&F_max, &Q, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    // Failure load
    Q *= q;
  }

};
