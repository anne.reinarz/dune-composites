#ifndef DISTRIBUTED_FIELD_HH
#define DISTRIBUTED_FIELD_HH

/**
 * @class DistributedField
 * @brief Provides distributed storage and access for a field that is
 *        constant on each grid cell
 *
 * Given a field which has been read in on processor zero, this class
 * allocates storage for the processor local portion of this field and copies
 * the local part of the field. On each processor the local part of the field
 * can then be accessed by a getter method.
 *
 * This only works for a structured axiparallel grid of size
 * \f$(L_x,L_y,L_z)\f$ with \f$n_x\times n_y\times n_z\f$ grid cells. On
 * each processor only data on the owned (and halo) cells in the range
 * \f$ [n_x^{(\min)},n_x^{(\max)}] \times
 *     [n_y^{(\min)},n_y^{(\max)}] \times
 *     [n_z^{(\min)},n_z^{(\max)}]\f$
 * is stored, which reduces the storage requirements significantly.
 *
 * Data is stored in lexicographic order as
 * \f$\phi_{i,j,k}=phi[i+nx*(j+ny*k)]\f$.
 *
 * Template parameters:
 * - GV type of gridview
 * - T type of field (usually float or double)
 */
template <class GV, class T>
class DistributedField {
  typedef typename GV::Grid Grid; // Grid
  static const int dim = Grid::dimension; // Dimension
  typedef typename GV::Traits::CollectiveCommunication Comm;
public:

  using F = typename GV::ctype;

  /** @brief Construct a new instance
   * Allocate space for local storage, work out index ranges and scatter
   * field to all processors. The global field can then be deleted.
   *
   * @param[in] gv_ Grid view
   * @param[in] gridsize_ Array \f$[n_x,n_y,n_z]\f$ with number of grid cells
   *            in each direction
   * @param[in] domainsize_ Array \f$[L_x,L_y,L_z]\f$ with extend of domain
   *            in each direction
   * @param[in] global_field Array with field values. This only needs to be
   *            available on processor 0
   */
  DistributedField(const GV& gv_,
                   const int gridsize_[dim],
                   const F domainsize_[dim],
                   const T* global_field
                   ) : gv(gv_), grid(gv_.grid()), comm(gv_.comm()) {
    std::copy(gridsize_,gridsize_+dim,gridsize);
    std::copy(domainsize_,domainsize_+dim,domainsize);
    for (int i=0;i<dim;++i) {
      cellsize[i] = domainsize[i]/gridsize[i];
    }
    // Work out the index ranges stored on each processor
    determineIndexRanges();
    // Work out number of (local) cells
    ncell = 1;
    for (int i=0;i<dim;++i) ncell *= n_local[i];
    // Allocate local field
    field = (T*) malloc(sizeof(T)*ncell);
    // Distribute field to all processors
    scatterField(global_field);
  }

  /** @brief Destructor
   * Deallocate storage
   */
  ~DistributedField() {
    free(field);
  }

  /** @brief Get data at particular position
   * Only works if the position vector is in the processor local part
   * of the domain
   *
   * @param x Position vector
   */
  template <class V>
  T get(const V& x) const {
    // Work out linear index lin_idx i+nx*(j+ny*k)
    int lin_idx = 0;
    for (int i=dim-1;i>=0;--i) {
      lin_idx *= n_local[i];
      lin_idx += static_cast<int>(x[i]/cellsize[i]) - min_idx[i];
    }
    return field[lin_idx];
  }

 private:

  /** @brief Scatter global field to processors
   * Copy the part of the global field which should be send to a specific
   * processor into a contiguous region of a scatter buffer and then call
   * scatterv on this buffer.
   */
  void scatterField(const T* global_field) {
    // Number of processors
    int nproc = comm.size();
    // minimal index in each direction on each processor
    int* min_idx_global = (int*) malloc(sizeof(int)*dim*nproc);
    // maximal index in each direction on each processor
    int* max_idx_global = (int*) malloc(sizeof(int)*dim*nproc);
    // displacements and sendcounts for scatterv
    int* displacements = (int*) malloc(sizeof(int)*nproc);
    int* sendcounts = (int*) malloc(sizeof(int)*nproc);
    // gather min- and max-information of all processes on master
    // min_idx_global[dim*rank+i] = min_idx(rank,i)
    // max_idx_global[dim*rank+i] = max_idx(rank,i)
    comm.allgather(min_idx,dim,min_idx_global);
    comm.allgather(max_idx,dim,max_idx_global);
    T* scatter_buffer;
    // Work out total number of cells with halos
    int ncell_with_halos = comm.sum(ncell);
    if (comm.rank()==0) {
      // Copy data in all subdomains into a contiguous section
      // of the scatter buffer (note that this is only allocated on the
      // master process)
      scatter_buffer = (T*) malloc(sizeof(T)*ncell_with_halos);
      int copy_offset = 0;
      for(int rank=0;rank<nproc;++rank) {
        displacements[rank] = copy_offset;
        sendcounts[rank] = 0;
        // Data is contiguous in the i-direction, so we always copy
        // max - min + 1 entries
        int length=max_idx_global[dim*rank+0]-min_idx_global[dim*rank+0]+1;
        // Loop over all k-indices
        for (int k=min_idx_global[dim*rank+2];
             k<=max_idx_global[dim*rank+2];++k) {
          // Loop over all j-indices
          for (int j=min_idx_global[dim*rank+1];
               j<=max_idx_global[dim*rank+1];++j) {
            // Work out index in global array
            int offset = gridsize[0]*(j+gridsize[1]*k)+min_idx_global[dim*rank+0];
            // Copy to contiguous array
            std::copy(global_field+offset,global_field+offset+length,
                      scatter_buffer+copy_offset);
            copy_offset += length;
            sendcounts[rank] += length;
          }
        }
      }
    }
    // Scatter the data in the reshuffled array
    comm.scatterv(scatter_buffer,sendcounts,displacements,
                  field,ncell,0);
    // Free memory
    if (comm.rank() == 0) {
      // This buffer was only allocated on the master!
      free(scatter_buffer);
    }
    free(min_idx_global);
    free(max_idx_global);
    free(displacements);
    free(sendcounts);
  }

  /** @brief Work out \f$ [n_x^{(\min)},n_x^{(\max)}] \times
   *                      [n_y^{(\min)},n_y^{(\max)}] \times
   *                      [n_z^{(\min)},n_z^{(\max)}]\f$
   *
   * On each processor calculate the index range in all dimensions.
   * This is done by looping over this grid and calculating all indices
   * which are reached by the griditeration.
   */
  void determineIndexRanges() {
    std::fill(min_idx,min_idx+dim,1000000);
    std::fill(max_idx,max_idx+dim,-1000000);
    typedef typename Grid::template Codim<0>::template Partition<Dune::All_Partition>::LeafIterator ElementLeafIterator;
    ElementLeafIterator ibegin = grid.template leafbegin<0>();
    ElementLeafIterator iend = grid.template leafend<0>();
    for (ElementLeafIterator it = ibegin;it!=iend;++it) {
      auto geo = it->geometry();
      auto center = geo.center();
      for (int i=0;i<dim;++i) {
        int idx = static_cast<int>(static_cast<double>(center[i]/cellsize[i]));
        min_idx[i] = std::min(idx,min_idx[i]);
        max_idx[i] = std::max(idx,max_idx[i]);
      }
    }
    for (int i=0;i<dim;++i) {
      n_local[i] = max_idx[i]-min_idx[i]+1;
    }
  }
private:
  const GV& gv; /** Gridview */
  const Grid& grid; /** Grid */
  const Comm& comm; /** Collective communicator (extracted from grid) */
  int gridsize[dim]; /** (Global) number of cells in each dimension \f$[n_x,n_y,n_z]\f$*/
  F domainsize[dim]; /** Size of domain in each dimension \f$[L_x,L_y,L_z]\f$*/
  F cellsize[dim]; /** Size of cell in each dimension \f$h_i = L_i/n_i\f$*/
  int ncell_global; /** Number of cells in global grid */
  int ncell; /** Number of cells in local grid stored on each processor */
  int min_idx[dim]; /** Smallest grid cell index in all dimensions */
  int max_idx[dim]; /** Largest grid cell index in all dimensions */
  int n_local[dim]; /** Number of local cells in each dimension */
  T* field; /** The local field data */
};

#endif // DISTRIBUTED_FIELD_HH
