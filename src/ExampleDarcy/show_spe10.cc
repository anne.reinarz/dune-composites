// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string>
#include <math.h>
#include<dune/grid/yaspgrid.hh>
#include<dune/grid/io/file/vtk/vtkwriter.hh>

// Dimension
const int dim=3;
// Grid sizes
const int nx=60, ny=220, nz=85;
// Domain sizes
const double Lx=1200.0, Ly=2200.0, Lz=170.0;
// Total number of cells
const int N=nx*ny*nz;

/* ********************************************************************* *
 * Read SPE10 file from disk and dump to vtu file
 * ********************************************************************* */

/** Read a particular field */
void read_field(FILE* ptr_file, // File pointer
                std::vector<double>& field,   // Resulting field
                const std::string label) {
  field.resize(N);
  float min,max;
  min=1e30; max=-1e30;
  float mean = 0.0;
  for (int k=0; k<nz; k++)
    for (int j=0; j<ny; j++)
      for (int i=0; i<nx; i++)
        {
          float perm;
          if (fscanf(ptr_file,"%g",&perm)!=EOF)
            {
              field[(nz-1-k)*nx*ny+j*nx+i] = perm;
            }
          else
            {
              std::cout << label << ": unexpected EOF i=" << i << " j=" << j << " k=" << k << std::endl;
              DUNE_THROW(Dune::Exception,"spe10: unexpected end of file");
            }
          min=std::min(min,perm);
          max=std::max(max,perm);
          mean += perm;
        }
  mean /= N;
  std::cout << label << " : min=" << min << " max=" << max << " mean=" << mean << std::endl;
}

/** M A I N */
int main(int argc, char* argv[]) {
  //Maybe initialize Mpi
  Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
  if (not Dune::MPIHelper::isFake) {
    if (helper.size()>1) {
      if (helper.rank()==0) {
        std::cout << "ERROR: Can only run on one processor" << std::endl;
      }
      exit(1);
    }
  }

  if (argc != 2) {
    std::cout << "Usage: " << argv[0] << "<filename>" << std::endl;
    std::cout << "  Reads SPE10 data from specified file." << std::endl;
    exit(1);
  }
  // Get name of SPE10 data file to read
  std::string spe10filename = argv[1];

  // Create grid
  Dune::FieldVector<double,dim> L = {Lx,Ly,Lz};
  std::array<int,dim> Ncell = {nx,ny,nz};
  std::bitset<dim> P(false);
  typedef Dune::YaspGrid<dim,Dune::EquidistantCoordinates<double,dim> > Grid;
  int overlap=0;
  Grid grid(L,Ncell,P,overlap,Grid::CollectiveCommunicationType());
  typedef typename Grid::LeafGridView GV;
  const GV gv=grid.leafGridView();

  // Allocate data
  std::vector<std::vector<double> > data(dim);
  std::vector<std::string> labels(dim);
  labels[0] = "kx";
  labels[1] = "ky";
  labels[2] = "kz";

  // Read data from disk
  FILE *ptr_file;
  ptr_file = fopen(spe10filename.c_str(),"r");
  if (not ptr_file) {
    std::cout << "ERROR: cannot open SPE10 data file" << std::endl;
    exit(1);
  }
  std::cout << "start reading SPE10 data file" << std::endl;
  // Read file
  for (int i=0;i<dim;++i) {
    read_field(ptr_file,data[i],labels[i]);
  }
  fclose(ptr_file);
  std::cout << "read SPE10 data file successfully" << std::endl;

  Dune::VTKWriter<GV> vtkwriter(gv);
  for (int i=0;i<dim;++i) {
    std::cout << "size = " << data[i].size() << std::endl;
    vtkwriter.addCellData (data[i],labels[i]);
  }
  vtkwriter.write("spe10_permeability",Dune::VTK::appendedraw);
}
