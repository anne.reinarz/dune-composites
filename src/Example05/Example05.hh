#include<dune/composites/Setup/baseGridModel.hh>
#include <dune/composites/Driver/linearStaticDriver.hh>
#include <dune/composites/PostProcessing/FailureCriteria/camanhoCriterion.hh>



template<int DIM>
class Example05 : public Dune::Composites::baseGridModel< Dune::YaspGrid<3,Dune::TensorProductCoordinates<double,3>>>{

    public:

        double Q;
        int overlap;
        bool verbosity;

        Dune::Composites::linearStaticDriver<Example05<DIM>> myDriver;

        Example05(Dune::MPIHelper& helper_) : baseGridModel(helper_), myDriver(helper_){

            overlap = 1;
            verbosity = (helper.rank() == 0);

            if(helper.size() > 1){
                solverParameters.solver = "CG";
                solverParameters.preconditioner = "OneLevel_AdditiveSchwarz";
                solverParameters.subdomainSolver = "UMFPack";
            }
            else{
                solverParameters.solver = "UMFPack";
            }

            vtk_displacement_output = "Example05_displacement";
            vtk_stress_output = "Example05_stress";
            vtk_properties_output = "Example05_properties"; 

        };

    void inline apply(){
        myDriver.apply(*this);
    }

    void setUpMaterials(){
        numMaterials = 3;
        myMaterials.resize(numMaterials);

        // Material I: Isotropic Resin
        myMaterials[0].setType(0); // Isotropic
        myMaterials[0].setProp(0,8800.); // - E,  Young's Modulus
        myMaterials[0].setProp(1,0.314); // - nu,   Poisson Ratio       
        myMaterials[0].setDensity(1.57e-5); // - rho,  Density  kg/mm^2

        // Material II: Orthotropic, Composite Ply - AS4/8552
        myMaterials[1].setType(1); // Orthotropic Composite
        myMaterials[1].setProp(0,137100.); // E11
        myMaterials[1].setProp(1,8800.); // E22
        myMaterials[1].setProp(2,8800.); // E33
        myMaterials[1].setProp(3,0.314); // nu12
        myMaterials[1].setProp(4,0.314); // nu13
        myMaterials[1].setProp(5,0.487); // nu23
        myMaterials[1].setProp(6,4900.); // G12
        myMaterials[1].setProp(7,4900.); // G13
        myMaterials[1].setProp(8,2959.); // G23
        myMaterials[1].setDensity(1.57e-5); // - rho, Density kg/mm^2
    }


    bool inline isDirichlet(FieldVec& x, const int dof){
        //! Default isDirichlet function, returning homogeneous Dirichlet boundary conditions
        if(x[0] > 20 -1e-3 && dof == 0)
            return true;
        return (x[0] < 1e-6);
    }

    double inline evaluateDirichlet(const FieldVec& x, int dof) const{
                    // Return homogeneous boundary conditions
        if(x[0] > 20 -1e-3 && dof == 0)
            return 0.0277265;
        return 0.0;
    }


    inline void evaluateWeight(FieldVec& f, int id) const{
        //! body force, e.g. gravity
        f = 0.0;
    }


    double inline FailureCriteria(Dune::FieldVector<double,6>& stress) const{
        const std::vector<double> param = {61, 97, 94}; //material allowables in MPa
        std::vector<double> stress_std(6);
        for(int i= 0; i < 6; i++) stress_std[i] = stress[i];
        //TODO material type check
        return Dune::Composites::Camanho(stress_std, 0, param);
    }

    //! \fn A member taking which constructs the base layered laminate in untransformed coordinates
    void inline LayerCake(){
        std::string example05_Geometry = "stackingSequences/example5.csv";
        LayerCakeFromFile(example05_Geometry);
        int numRegions = 3;
        std::vector<Dune::FieldVector<double,3>> grading(numRegions);
        grading[0] = {1./10,1,1};
        grading[1] = {1,1,1};
        grading[2] = {10,1,1};
        std::vector<Dune::FieldVector<int,3>> directions(numRegions);
        directions[0] = {1,0,0}; //only grade mesh in x direction
        directions[0] = {0,0,0};
        directions[0] = {1,0,0};
        GeometryBuilder(directions,grading);
    }

    template<class GO, class U, class GFS, class C,class MBE, class GV>
    void inline postprocess(const GO& go, U& u, const GFS& gfs, const C& cg, const GV gv, MBE& mbe){
        using Dune::PDELab::Backend::native;
        double local_u1_max = 0.0;
        double local_fail_max = 0.0;
        for (unsigned int i = 0; i < native(u).size(); i++){ // Loop over each vertex
            double u1 = std::abs(native(u)[i][0]);
            if (local_u1_max < u1){ local_u1_max = u1; }
        }
        for (unsigned int i = 0; i < sig.size(); i++){ // Loop over each vertex
            Dune::FieldVector<double,6> stress = {sig[i][0], sig[i][1], sig[i][2], sig[i][3], sig[i][4], sig[i][5]};
            double fail = FailureCriteria(stress);
            if (local_fail_max < fail){ local_fail_max = fail; }
        }
        double u1_glob = 0.0;
        MPI_Allreduce(&local_u1_max, &u1_glob, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        std::cout << "Max displacement " << u1_glob << std::endl;
        MPI_Allreduce(&local_fail_max, &Q, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

        //Write vtk output of failure criterion
        typedef Dune::PDELab::ISTL::VectorBackend<> Scalar_VectorBackend;
        typedef typename GV::Grid::ctype e_ctype;
        typedef Dune::PDELab::QkDGLocalFiniteElementMap<e_ctype,double,0,3> FEM_stress;
        FEM_stress fem_stress;

        typedef Dune::PDELab::GridFunctionSpace<GV, FEM_stress, Dune::PDELab::NoConstraints, Scalar_VectorBackend> P1GFS;
        P1GFS p1gfs(gv,fem_stress); p1gfs.name("failureCriterion");
        typedef Dune::PDELab::Backend::Vector<P1GFS,double> V;
        V failure(p1gfs,0.0);

        for (unsigned int i = 0; i < native(failure).size(); i++){
            Dune::FieldVector<double,6> stress = {sig[i][0], sig[i][1], sig[i][2], sig[i][3], sig[i][4], sig[i][5]};
            native(failure)[i] = FailureCriteria(stress);
        }

        // Save solution in vtk format for paraview
        Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(0));
        Dune::PDELab::addSolutionToVTKWriter(vtkwriter,p1gfs,failure);
        vtkwriter.write("Example05_failureCriterion",Dune::VTK::appendedraw);
    }

};
