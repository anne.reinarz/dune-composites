// Creates l-shaped domain for dune-elastic
lay_num = 3;             //Set number of layers in composite
lay_res_h = 1/30;         //size of the resin layers
lay_com_h = 1/30;         //size of the composite layers  
//mesh_h = 0.25;           //meshing parameter

//Defect parameters
N = 10;  //Number of bspline control points
defect_size = 0.0;      //size of defect, choose zero to remove defect
defect_location = 1;      //fibre in which the defect is located
frequency = 0;            //frequency
dampingX = 1;             //damping in X
dampingY = 0;             //damping in Y
dampingZ = .8;             //damping in Z

limb_length = 1/3;
radius_inner = 2/3-1/10;

thickness = Ceil(lay_num/2)*lay_com_h+Floor(lay_num/2)*lay_res_h;

//Some more parameters
Y = .5;                   //Extension in Y direction, default 0.5
defect_locationY = Y/2;   //Defect location in Y, default middle


//Define points
cnt = 1;
loc[0] = 0.0;
bool = 1;
For i In {0:lay_num}
  Point(cnt)   = {0,0,loc[i]};
  pLO[i] = cnt;
  Point(cnt+1) = {thickness+limb_length+radius_inner-loc[i],0,thickness+radius_inner+limb_length};
  pUO[i] = cnt+1;
  Point(cnt+2) = {thickness+limb_length+radius_inner-loc[i],0,thickness+radius_inner};
  pUI[i] = cnt+2;
  Point(cnt+3) = {limb_length,0,loc[i]};
  pLI[i] = cnt+3;
  loc[i+1] = loc[i]+bool*lay_res_h+(1-bool)*lay_com_h;
  bool = Ceil(i/2) - Floor(i/2);  //layer switching
  cnt  = cnt + 4;
EndFor

//Connect lines
For i In {0:lay_num}
   Line(cnt)   = {pLO[i],pLI[i]};
   lLC[i] = cnt;
   Line(cnt+1) = {pUI[i],pUO[i]};
   lUC[i] = cnt+1;
   cnt = cnt + 2;
EndFor

For i In {0:lay_num-1}
  Line(cnt)   = {pLO[i],pLO[i+1]};
  lLO[i] = cnt;
  Line(cnt+1) = {pLI[i],pLI[i+1]};
  lLI[i] = cnt+1;
  Line(cnt+2) = {pUO[i],pUO[i+1]};
  lUO[i] = cnt+2;
  Line(cnt+3) = {pUI[i],pUI[i+1]};
  lUI[i] = cnt+3;
  cnt  = cnt + 4;
EndFor


//Define B-splines for "curvy bit"
y_val = 0.0;
For i In {0:lay_num}
   bspl_pts = cnt;
   For j In {1:N-1}
     theta = j/N;
     radius = thickness+radius_inner-loc[i] + defect_size * Cos(frequency*Pi*(2*theta-1)) 
                                          * (1/Cosh(dampingZ * 2*Pi*(defect_location-i)/lay_num)^2)
                                          * (1/Cosh(dampingX * Pi*(2*theta-1))^2)
                                          * (1/Cosh(dampingY * Pi*(defect_locationY - y_val))^2);
     Point(cnt) = {radius*Cos(Pi/2*theta-Pi/2)+limb_length,0,radius*Sin(Pi/2*theta-Pi/2)+radius_inner+thickness};
     cnt = cnt+1;
   EndFor
   BSpline(cnt) = {pLI[i],bspl_pts:(cnt-1), pUI[i]};
   bspl[i] = cnt;
   cnt = cnt + 1;
EndFor

//Define surfaces
For i In {0:lay_num-1}
  Line Loop(cnt) = {lLO[i],-lLC[i],-lLI[i],lLC[i+1]};
  Plane Surface(cnt+1) = {cnt};
  s01[i] = cnt+1;
  Line Loop(cnt+2) = {-lUI[i],lUC[i],lUO[i],-lUC[i+1]};
  Plane Surface(cnt+3) = {cnt+2};
  s02[i] = cnt+3;
  Line Loop(cnt+4) = {lLI[i],-bspl[i], -lUI[i], bspl[i+1]};
  Ruled Surface(cnt+5) = {cnt+4};
  s03[i] = cnt+5;
  cnt = cnt + 6;
EndFor

//Extrude geometry by hand to allow parameter changes in y-direction
//Define needed points
loc[0] = 0.0;
bool = 1;
For i In {0:lay_num}
  Point(cnt)   = {0,Y,loc[i]};
  pLOY[i] = cnt;
  Point(cnt+1) = {limb_length+thickness+radius_inner-loc[i],Y,limb_length+thickness+radius_inner};
  pUOY[i] = cnt+1;
  Point(cnt+2) = {limb_length+thickness+radius_inner-loc[i],Y,thickness+radius_inner};
  pUIY[i] = cnt+2;
  Point(cnt+3) = {limb_length,Y,loc[i]};
  pLIY[i] = cnt+3;
  loc[i+1] = loc[i]+bool*lay_res_h+(1-bool)*lay_com_h;
  bool = Ceil(i/2) - Floor(i/2);  //layer switching
  cnt  = cnt + 4;
EndFor

//Connect lines
For i In {0:lay_num}
   Line(cnt)   = {pLOY[i],pLIY[i]};
   lLCY[i] = cnt;
   Line(cnt+1) = {pUIY[i],pUOY[i]};
   lUCY[i] = cnt+1;
   cnt = cnt + 2;
EndFor

For i In {0:lay_num-1}
  Line(cnt)   = {pLOY[i],pLOY[i+1]};
  lLOY[i] = cnt;
  Line(cnt+1) = {pLIY[i],pLIY[i+1]};
  lLIY[i] = cnt+1;
  Line(cnt+2) = {pUOY[i],pUOY[i+1]};
  lUOY[i] = cnt+2;
  Line(cnt+3) = {pUIY[i],pUIY[i+1]};
  lUIY[i] = cnt+3;
  cnt  = cnt + 4;
EndFor

//Define B-splines for "curvy bit"
y_val = Y;
For i In {0:lay_num}
   bspl_pts = cnt;
   For j In {1:N-1}
     theta = j/N;
     radius = radius_inner + thickness-loc[i] + defect_size * Cos(frequency*Pi*(2*theta-1)) 
                                          * (1/Cosh(dampingZ * 2*Pi*(defect_location-i)/lay_num)^2)
                                          * (1/Cosh(dampingX * Pi*(2*theta-1))^2)
                                          * (1/Cosh(dampingY * Pi*(defect_locationY - y_val))^2);
     Point(cnt) = {radius*Cos(Pi/2*theta-Pi/2)+limb_length,Y,radius*Sin(Pi/2*theta-Pi/2)+radius_inner+thickness};
     cnt = cnt+1;
   EndFor
   BSpline(cnt) = {pLIY[i],bspl_pts:(cnt-1), pUIY[i]};
   bsplY[i] = cnt;
   cnt = cnt + 1;
EndFor

//Lines across
For i In {0:lay_num}
  Line(cnt)   = {pLOY[i],pLO[i]};
  lYLO[i] = cnt;
  Line(cnt+1) = {pLIY[i],pLI[i]};
  lYLI[i] = cnt+1;
  Line(cnt+2) = {pUOY[i],pUO[i]};
  lYUO[i] = cnt+2;
  Line(cnt+3) = {pUIY[i],pUI[i]};
  lYUI[i] = cnt+3;
  cnt  = cnt + 4;
EndFor

//New surfaces
For i In {0:lay_num-1}
  Line Loop(cnt) = {lLOY[i],-lLCY[i],-lLIY[i],lLCY[i+1]};
  Plane Surface(cnt+1) = {cnt};
  sY1[i] = cnt+1;
  Line Loop(cnt+2) = {-lUIY[i],lUCY[i],lUOY[i],-lUCY[i+1]};
  Plane Surface(cnt+3) = {cnt+2};
  sY2[i] = cnt+3;
  Line Loop(cnt+4) = {lLIY[i],-bsplY[i], -lUIY[i], bsplY[i+1]};
  Ruled Surface(cnt+5) = {cnt+4};
  sY3[i] = cnt+5;
  cnt = cnt + 6;
EndFor

For i In {0:lay_num-1}
  Line Loop(cnt) = {lYLO[i], -lLOY[i],-lYLO[i+1], lLO[i]};
  Plane Surface(cnt+1) = {cnt};
  sYLO[i] = cnt+1;
  Line Loop(cnt+2) = {lYLI[i], -lLIY[i], -lYLI[i+1], lLI[i]};
  Plane Surface(cnt+3) = {cnt+2};
  sYLI[i] = cnt+3;
  Line Loop(cnt+4) = {lYUO[i], -lUOY[i],-lYUO[i+1], lUO[i]};
  Ruled Surface(cnt+5) = {cnt+4};
  sYUO[i] = cnt+5;
  Line Loop(cnt+6) = {lYUI[i], -lUIY[i],-lYUI[i+1], lUI[i]};
  Ruled Surface(cnt+7) = {cnt+6};
  sYUI[i] = cnt+7;
  cnt = cnt + 8;
EndFor

For i In {0:lay_num}
  Line Loop(cnt) = {lYLO[i], lLC[i], -lYLI[i], -lLCY[i]};
  Plane Surface(cnt+1) = {cnt};
  sYT1[i] = cnt+1;
  Line Loop(cnt+2) = {lYLI[i], bspl[i], -lYUI[i], -bsplY[i]};
  Ruled Surface(cnt+3) = {cnt+2};
  sYT2[i] = cnt+3;
  Line Loop(cnt+4) = {lYUI[i], lUC[i], -lYUO[i], -lUCY[i]};
  Plane Surface(cnt+5) = {cnt+4};
  sYT3[i] = cnt+5;
  cnt = cnt + 6;  
EndFor

//Define Volumes and Physical Groups
For i In {0:lay_num-1}
  Surface Loop(cnt) = {s01[i],sYLO[i],sY1[i],sYLI[i],sYT1[i],sYT1[i+1]};
  Volume(cnt+1) = {cnt};
  Physical Volume(100+i) = cnt+1;
  Surface Loop(cnt+2) = {s03[i],sYLI[i],sY3[i],sYUI[i],sYT2[i],sYT2[i+1]};
  Volume(cnt+3) = {cnt+2};
  Physical Volume(200+i) = cnt+3;
  Surface Loop(cnt+4) = {s02[i],sYUI[i],sY2[i],sYUO[i],sYT3[i],sYT3[i+1]};
  Volume(cnt+5) = {cnt+4};
  Physical Volume(300+i) = cnt+5;
  cnt = cnt + 6;
EndFor
