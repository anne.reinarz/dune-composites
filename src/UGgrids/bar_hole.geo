Point(1) = {0, 0, 0, 0.1};
Point(2) = {1, 0, 0, 0.1};
Point(3) = {0, 1, 0, 0.1};
Point(4) = {1, 1, 0, 0.1};

Line(1) = {1,2};
Line(2) = {2,4};
Line(3) = {4,3};
Line(4) = {3,1};

For i In {1:100}
    Point(i+4) = {0.5+0.1*Sin(Pi*2*i/100),0.5+0.1*Cos(Pi*2*i/100),0,0.1};
EndFor

BSpline(5) = {5:52};
BSpline(6) = {52:104,5};

Line Loop(7) = {4, 1, 2, 3};
Line Loop(8) = {6, 5};

Ruled Surface(9) = {7, 8};
Extrude {0, 0, .1} {
  Surface{9};
}
Surface Loop(42) = {41, 20, 9, 24, 28, 32, 36, 40};
Volume(43) = {42};
Physical Volume(44) = {100};
