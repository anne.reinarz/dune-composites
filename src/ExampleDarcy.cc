#ifndef PROBLEM_SP10
#define PROBLEM_SP10

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include <dune/pdelab/boilerplate/pdelab.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionadapter.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include "ExampleDarcy/distributed_field.hh"
#include <random>
#include <algorithm>

#include <dune/pdelab/backend/istl/geneo/geneo.hh>

#include<dune/common/parametertree.hh>
#include<dune/common/parametertreeparser.hh>

template<typename GV, typename RF>
class ProblemSPE10
{
public:
  /** \brief Can we assume that the underlying geometry is affine? */
  static constexpr bool affine_transformation=true;
  /** \brief Axis parallel? */
  static constexpr bool axis_parallel=true;
  /** \brief Is the diffusion tensor constant over the domain? */
  static constexpr bool constant_A=true;
  /** \brief Is the diffusion tensor zero? */
  static constexpr bool zero_A=false;
  /** \brief Is the velocity field zero?*/
  static constexpr bool zero_b=true;
  /** \brief is the velocity field constant? */
  static constexpr bool constant_b=true;
  /** \brief Is the sink term zero?*/
  static constexpr bool zero_c=true;
  /** \brief Is the sink term constant?*/
  static constexpr bool constant_c=true;
  /** \brief Is the source term zero?*/
  static constexpr bool zero_f=true;
  /** \brief Sum factorisation coordinates */
  static constexpr bool sumfact_coords=true;

  static constexpr bool diagonal_A = true;

  /** \brief Is the problem symmetric? */
  static constexpr bool symmetric=true;

  //! tensor diffusion constant per cell? return false if you want more than one evaluation of A per cell.
  static constexpr bool permeabilityIsConstantPerCell()
  {
    return true;
  }

  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  /** \brief dimension */
  static constexpr std::size_t dim = Traits::dimDomain;

  typedef typename GV::Traits::Communication Comm;
  ProblemSPE10 (const GV& gv_,
                Dune::ParameterTree& param_grid_,
                Dune::ParameterTree& param_spe10_,
                const bool debug=false) :
    gv(gv_), comm(gv.grid().comm()),
    param_grid(param_grid_), param_spe10(param_spe10_)
  {
    // works only in 3d
    if (Traits::dimDomain!=3)
        DUNE_THROW(Dune::Exception,"spe10: dim must be 3");

    // read structured grid parameters
    LX = param_grid.get<RF>("Lx");
    LY = param_grid.get<RF>("Ly");
    LZ = param_grid.get<RF>("Lz");
    int refinement = param_grid.get("refinement",(int)0);
    NX = param_grid.get<int>("nx") * (1 << refinement);
    NY = param_grid.get<int>("ny") * (1 << refinement);
    NZ = param_grid.get<int>("nz") * (1 << refinement);

    scaling = param_spe10.get<RF>("scaling",(RF)1.0);

    hx = LX/NX;
    hy = LY/NY;
    hz = LZ/NZ;

    std::string spe10filename = param_spe10.get<std::string>("filename");

    //if (comm.rank()==0) {
      kx_global = (float*) malloc(sizeof(float)*N);
      ky_global = (float*) malloc(sizeof(float)*N);
      kz_global = (float*) malloc(sizeof(float)*N);
        FILE *ptr_file;
        ptr_file = fopen(spe10filename.c_str(),"r");
        if (!ptr_file)
            DUNE_THROW(Dune::Exception,"spe10: cannot open data file");
        std::cout << "start reading SPE10 data file" << std::endl;
        // read kx, ky and kz
        read_field(ptr_file,kx_global,"kx");
        read_field(ptr_file,ky_global,"ky");
        read_field(ptr_file,kz_global,"kz");
        fclose(ptr_file);
        std::cout << "read data file successfully" << std::endl;
    //}

    // Create distributed field
    const int gridsize[3] = {NX, NY, NZ};
    const RF domainsize[3] = {LX, LY, LZ};

    for (std::size_t i=0;i<dim;++i)
    {
      cellsize[i] = domainsize[i]/gridsize[i];
    }
  }

  /*const std::shared_ptr<DistributedField<GV,float>> getKx() const {
    return kx;
  }*/

  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType center = e.geometry().center();
    typename Traits::PermTensorType I(0.0);
    I[0][0] = eval_field(kx_global, center); //kx->get(center);
    I[1][1] = eval_field(ky_global, center); //ky->get(center);
    I[2][2] = eval_field(kz_global, center); //kz->get(center);
    return I;
  }

  /** \brief label */
  std::string label() const { return "spe10"; }

  /** \brief Is this operator symmetric? */
  bool is_symmetric() const { return symmetric; }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType v(0.0);
    return v;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 1.0;
  }


  //! boundary condition type function
  /* return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet for Dirichlet boundary conditions
   * return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann for flux boundary conditions
   * return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Outflow for outflow boundary conditions
   */
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    typename Traits::DomainType xglobal = is.geometry().global(x);
    if (xglobal[2]<1e-6)
	return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
    //if (xglobal[2]>LZ-1e-6)
    //	return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
    return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(xlocal);
    return xglobal[2] < 1e-6 ? 0.0 : 0.0;
  }

  //! flux boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! outflow boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

private:

  float eval_field(float* field, typename Traits::DomainType xglobal) const {
    int k = xglobal[2] / cellsize[2];
    int j = xglobal[1] / cellsize[1];
    int i = xglobal[0] / cellsize[0];
    return field[(nz-1-k)*nx*ny+j*nx+i];
  }

  /** Read field from SPE10 data file
   */
  void read_field(FILE* ptr_file, // File pointer
                  float* field,   // Resulting field
                  const std::string label) {
    float min,max;
    min=1e30; max=-1e30;
    float mean = 0.0;
    for (int k=0; k<nz; k++)
      for (int j=0; j<ny; j++)
        for (int i=0; i<nx; i++)
          {
            float perm;
            if (fscanf(ptr_file,"%g",&perm)!=EOF)
              {
                field[(nz-1-k)*nx*ny+j*nx+i] = perm;
              }
            else
              {
                std::cout << label << ": unexpected EOF i=" << i << " j=" << j << " k=" << k << std::endl;
                DUNE_THROW(Dune::Exception,"spe10: unexpected end of file");
              }
            min=std::min(min,perm);
            max=std::max(max,perm);
            mean += perm;
          }
    mean /= N;
    std::cout << label << " : min=" << min << " max=" << max << " mean=" << mean << std::endl;
  }

  float* kx_global;
  float* ky_global;
  float* kz_global;

  RF LX,LY,LZ; /** \brief Size of domain (in ft) */
  int NX,NY,NZ; /** \brief Number of grid cells */
  RF hx,hy,hz; /** \brief size of grid cells hi = Li/Ni */
  RF gradx,grady; /** \brief gradient parameter */

  RF cellsize[dim]; /** Size of cell in each dimension \f$h_i = L_i/n_i\f$*/

  const int nx=60, ny=220, nz=85; // Grid sizes
  const int N=nx*ny*nz;
  RF scaling;
  RF kx_mean, ky_mean, kz_mean;
  const GV gv;
  const Comm& comm;
  Dune::ParameterTree& param_grid;
  Dune::ParameterTree& param_spe10;
};

template <typename GV, typename RF, int dim, typename Problem>
class ParameterGridFunction :
public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, ParameterGridFunction<GV,RF,dim,Problem> >
{

public:

  typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

  ParameterGridFunction (const GV& gv, const Problem& problem, int param_dim) : _problem(problem), _param_dim(param_dim) {}


  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
                        {
                          y = _problem.A(e,xlocal)[_param_dim][_param_dim];
                          return;
                        }

private:

  const Problem& _problem;
  const int _param_dim;
};


void driver(Dune::MPIHelper& helper) {

  const int dim = 3;
  const int degree = 1;

  std::string param_filename("ExampleDarcy/parameters.ini");
  Dune::ParameterTree param;
  Dune::ParameterTreeParser::readINITree(param_filename,param);
  Dune::ParameterTree& param_grid=param.sub("grid");

  double domainsize[3];
  int gridsize[3];
  int gridrefinement;

  gridsize[0]=param_grid.get<int>("nx");
  gridsize[1]=param_grid.get<int>("ny");
  gridsize[2]=param_grid.get<int>("nz");

  domainsize[0]=param_grid.get<double>("Lx");
  domainsize[1]=param_grid.get<double>("Ly");
  domainsize[2]=param_grid.get<double>("Lz");

  gridrefinement=param_grid.get<int>("refinement");

  int ncell=1;
  int ndof_per_cell=1;
  for (int i=0;i<dim;++i) {
    ncell *= (gridsize[i]) * (1 << gridrefinement);
    ndof_per_cell *= (degree+1);
  }

  int procsize[3];
  procsize[0] = param_grid.get<int>("px",(int)1);
  procsize[1] = param_grid.get<int>("py",(int)1);
  procsize[2] = param_grid.get<int>("pz",(int)1);

  Dune::ParameterTree& param_spe10=param.sub("spe10");

  using F = double;

  Dune::FieldVector<F,dim> L = {domainsize[0], domainsize[1], domainsize[2]};
  std::array<int,dim> Ncell = {gridsize[0], gridsize[1], gridsize[2]};
  std::bitset<dim> P(false);
  typedef Dune::YaspGrid<dim> Grid;

  int overlap = 1;
  std::array<int,dim> proc_dims = {procsize[0], procsize[1], procsize[2]};
  typedef Dune::Yasp::FixedSizePartitioning<dim> Partitioner;
  Partitioner partitioner(proc_dims);
  auto grid = std::make_shared<Grid>(L,Ncell,P,overlap,Grid::Communication(), &partitioner);

  if (gridrefinement > 0)
      grid->globalRefine(gridrefinement);

  typedef ProblemSPE10<Grid::LevelGridView,F> PROBLEM;
  PROBLEM problem(grid->levelGridView(grid->maxLevel()),param_grid, param_spe10);

  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<PROBLEM> BCType;
  BCType bctype(grid->levelGridView(grid->maxLevel()),problem);

  typedef typename Grid::LevelGridView::Grid::ctype DF;
  // instantiate finite element maps
  typedef Dune::PDELab::QkLocalFiniteElementMap<Grid::LevelGridView,DF,F,1> FEM;
  FEM fem(grid->levelGridView(grid->maxLevel()));

  // make function space, with overlapping constraints as usual for overlapping Schwarz methods
  typedef Dune::PDELab::GridFunctionSpace<Grid::LevelGridView,FEM,
                                          Dune::PDELab::OverlappingConformingDirichletConstraints,
                                          Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed,1>
                                          > GFS;
  GFS gfs(grid->levelGridView(grid->maxLevel()),fem);
  gfs.name("solution");

  // and a second function space with no constraints on processor boundaries, needed for the GenEO eigenproblem
  typedef Dune::PDELab::GridFunctionSpace<Grid::LevelGridView,FEM,
                                          Dune::PDELab::ConformingDirichletConstraints,
                                          Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed,1>
                                          > GFS_EXTERIOR;
  GFS_EXTERIOR gfs_exterior(grid->levelGridView(grid->maxLevel()),fem);


  // make a degree of freedom vector on fine grid and initialize it with interpolation of Dirichlet condition
  typedef Dune::PDELab::Backend::Vector<GFS,F> V;
  V x(gfs,0.0);

  // also create a wrapper using the same data based on the GFS without processor boundary constraints
  typedef Dune::PDELab::Backend::Vector<GFS_EXTERIOR,F> V_EXTERIOR;
  V_EXTERIOR x_exterior(gfs_exterior, Dune::PDELab::Backend::unattached_container());
  x_exterior.attach(x.storage());

  // Extract domain boundary constraints from problem definition, apply trace to solution vector
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<PROBLEM> G;
  G g(grid->levelGridView(grid->maxLevel()),problem);
  Dune::PDELab::interpolate(g,gfs,x);

  // Set up constraints containers
  //  - with boundary constraints and processor constraints as usual
  //  - with boundary constraints, but without processor constraints for the eigenprobleme
  //  - with only Neumann boundary constraints, but with processor constraints as needed for the partition of unity
  typedef typename GFS::template ConstraintsContainer<F>::Type CC;
  auto cc = CC();
  typedef typename GFS_EXTERIOR::template ConstraintsContainer<F>::Type CC_EXTERIOR;
  auto cc_exterior = CC_EXTERIOR();
  auto cc_bnd_neu_int_dir = CC();

  // assemble constraints
  Dune::PDELab::constraints(bctype,gfs,cc);
  Dune::PDELab::constraints(bctype,gfs_exterior,cc_exterior);

  Dune::PDELab::NoDirichletConstraintsParameters pnbc;
  Dune::PDELab::constraints(pnbc,gfs,cc_bnd_neu_int_dir);

  // set initial guess
  V x0(gfs,0.0);
  Dune::PDELab::copy_nonconstrained_dofs(cc,x0,x);

  // LocalOperator for given problem
  typedef Dune::PDELab::ConvectionDiffusionFEM<PROBLEM,FEM> LOP;
  LOP lop(problem);

  // LocalOperator wrapper zeroing out subdomains' interiors in order to set up overlap matrix
  typedef Dune::PDELab::LocalOperatorOvlpRegion<LOP, GFS> LOP_OVLP;
  LOP_OVLP lop_ovlp(lop, gfs);

  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;

  // Construct GridOperators from LocalOperators
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,F,F,F,CC,CC> GO;
  const std::size_t nonzeros = std::pow(2*degree+1,dim);
  auto go = GO(gfs,cc,gfs,cc,lop,MBE(nonzeros));

  typedef Dune::PDELab::GridOperator<GFS_EXTERIOR,GFS_EXTERIOR,LOP,MBE,F,F,F,CC,CC> GO_EXTERIOR;
  auto go_exterior = GO_EXTERIOR(gfs_exterior,cc_exterior,gfs_exterior,cc_exterior,lop,MBE(nonzeros));

  typedef Dune::PDELab::GridOperator<GFS_EXTERIOR,GFS_EXTERIOR,LOP_OVLP,MBE,F,F,F,CC,CC> GO_OVLP;
  auto go_overlap = GO_OVLP(gfs_exterior,cc_exterior,gfs_exterior,cc_exterior,lop_ovlp,MBE(nonzeros));

  // set up and assemble right hand side w.r.t. l(v)-a(u_g,v)
  V d(gfs,0.0);
  go.residual(x,d);

  // types
  typedef GO::Jacobian M;
  typedef GO_EXTERIOR::Jacobian M_EXTERIOR;

  // fine grid objects
  M AF(go);
  go.jacobian(x,AF);
  typedef Dune::PDELab::OverlappingOperator<CC,M,V,V> POP;
  auto popf = std::make_shared<POP>(cc,AF);
  typedef Dune::PDELab::ISTL::ParallelHelper<GFS> PIH;
  PIH pihf(gfs);
  typedef Dune::PDELab::OverlappingScalarProduct<GFS,V> OSP;
  OSP ospf(gfs,pihf);

  // Assemble fine grid matrix defined without processor constraints
  M_EXTERIOR AF_exterior(go_exterior);
  go_exterior.jacobian(x_exterior,AF_exterior);

  // Assemble fine grid matrix defined only on overlap region
  M_EXTERIOR AF_ovlp(go_overlap);
  go_overlap.jacobian(x_exterior,AF_ovlp);


  // Choose an eigenvalue threshold according to Spillane et al., 2014.
  // This particular choice is a heuristic working very well for Darcy problems.
  // Theoretically, any value delivers robustness; practically, you may need to
  // choose another value to achieve a good balance between condition bound and
  // global basis size
  Dune::ParameterTree& param_solver=param.sub("solver");
  if(param_solver.get<std::string>("solver") == "GenEO"){
      double evthreshold = param_solver.get<double>("evthreshold");
      double eigenvalue_threshold = evthreshold * (double)overlap / (220 + overlap); //nz=220

      // Create a local function space needed for the Sarkis partition of unity only
      typedef Dune::PDELab::LocalFunctionSpace<GFS, Dune::PDELab::AnySpaceTag> LFS;
      LFS lfs(gfs);

      // Generate a partition of unity
      Dune::Timer watch;
      watch.reset();
      std::shared_ptr<V> part_unity;
      part_unity = std::make_shared<V>(standardPartitionOfUnity<V>(gfs, cc_bnd_neu_int_dir));

      // Choose how many eigenvalues to compute
      int nev = param_solver.get<int>("nev");
      int nev_arpack = param_solver.get<int>("nev_arpack");

      // Construct per-subdomain basis functions
      std::shared_ptr<Dune::PDELab::SubdomainBasis<V> > subdomain_basis;
      subdomain_basis = std::make_shared<Dune::PDELab::GenEOBasis<GFS,M_EXTERIOR,V,1> >(gfs, AF_exterior, AF_ovlp, eigenvalue_threshold, *part_unity, nev, nev_arpack, 0.0001, false, 1);

      // FIXME: This breaks if #EV not constant across subdomains!!
      /*std::cout << "basis output..." << std::endl;
      for (int i = 0; i < subdomain_basis->basis_size(); i++) {

          Dune::SubsamplingVTKWriter<Grid::LevelGridView> vtkwriter(grid->levelGridView(grid->maxLevel()),Dune::refinementLevels(0));
          Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,*(subdomain_basis->get_basis_vector(i)));
          vtkwriter.write("basis_fct" + std::to_string(i),Dune::VTK::appendedraw);
      }
      std::cout << "basis output done" << std::endl;*/


      // Fuse per-subdomain basis functions to a global coarse space
      auto coarse_space = std::make_shared<Dune::PDELab::SubdomainProjectedCoarseSpace<GFS,M_EXTERIOR,V,PIH> >(gfs, AF_exterior, subdomain_basis, pihf);

      // Plug coarse basis into actual preconditioner
      auto prec = std::make_shared<Dune::PDELab::ISTL::TwoLevelOverlappingAdditiveSchwarz<GFS,M,V,V>>(gfs, AF, coarse_space, true, helper.rank()==0 ? 3:0);

      if(helper.rank()==0) std::cout << "Time for setup " << watch.elapsed() << std::endl;

      // now solve defect equation A*v = d using a CG solver with our shiny preconditioner
      V v(gfs,0.0);
      auto solver_ref = std::make_shared<Dune::CGSolver<V> >(*popf,ospf,*prec,1e-6,1000, helper.rank()==0 ? 2 : 0);
      Dune::InverseOperatorResult result;
      solver_ref->apply(v,d,result);
      x -= v;
  }
  if(param_solver.get<std::string>("solver") == "1level"){
#if HAVE_SUITESPARSE
      typedef Dune::PDELab::ISTLBackend_OVLP_CG_UMFPack<GFS, CC> PAR_UMF;  //CG with UMFPack
      PAR_UMF ls(gfs,cc,500,2);
      Dune::PDELab::StationaryLinearProblemSolver<GO,PAR_UMF,V> slp(go,ls,x,1e-6);
      slp.apply();
#endif
  }
  if(param_solver.get<std::string>("solver") == "ILU"){
      typedef Dune::PDELab::ISTLBackend_OVLP_GMRES_ILU0<GFS,CC> OVLP_GMRES_LS;
      OVLP_GMRES_LS ls(gfs,cc,5000,2);
      Dune::PDELab::StationaryLinearProblemSolver<GO,OVLP_GMRES_LS,V> slp(go,ls,x,1e-6);
      slp.apply();
  }
  if(param_solver.get<std::string>("solver") == "AMG"){
      typedef Dune::PDELab::ISTLBackend_CG_AMG_SSOR<GO> OVLP_AMG;
      OVLP_AMG ls(gfs,5000,2,true,false);
      Dune::PDELab::StationaryLinearProblemSolver<GO,OVLP_AMG,V> slp(go,ls,x,1e-6);
      slp.apply();
  }

  // Write solution to VTK
  if(helper.rank() == 0) std::cout << "Start writing vtk output" << std::endl;
  Dune::SubsamplingVTKWriter<Grid::LevelGridView> vtkwriter(grid->levelGridView(grid->maxLevel()),Dune::refinementLevels(0));
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,x);
  vtkwriter.write("spe10",Dune::VTK::appendedraw);
}


int main(int argc, char **argv)
{
  using Dune::PDELab::Backend::native;

  try{
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if(Dune::MPIHelper::isFake)
        std::cout<< "This is a sequential program." << std::endl;
    else{
        if(helper.rank()==0)
            std::cout << "parallel run on " << helper.size() << " process(es)" << std::endl;
    }

    driver(helper);

    return 0;
  }

  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }

}




#endif // PROBLEMSPE10_HH
