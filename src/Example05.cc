#include <dune/composites/composites.hh>

#include "Example05/Example05.hh"

//===============================================================
// Main program with grid initialiseSolve
//===============================================================

int main(int argc, char** argv){

	const int DIM = 3;

	// Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    Example05<DIM> myModel(helper);

    myModel.apply();

    if(helper.rank() == 0) std::cout << "QoI = " << myModel.Q << std::endl;
}
