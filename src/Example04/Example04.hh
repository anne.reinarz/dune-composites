// -*- tab-width: 4; indent-tabs-mode: nil -*-
// vi: set et ts=4 sw=4 sts=4:
#include <dune/composites/Setup/baseGridModel.hh>
#include <dune/composites/Driver/linearStaticDriver.hh>

template<int DIM>
class Example04 : public Dune::Composites::baseGridModel< Dune::YaspGrid<3,Dune::TensorProductCoordinates<double,3>>>{

    public:

        double Q;

        int overlap;
        bool verbosity;

        double A, lambda1, lambda2, lambda3, xstar, ystar, delta;
        Dune::Composites::linearStaticDriver<Example04<DIM>> myDriver;

        Example04(Dune::MPIHelper& helper_) : baseGridModel(helper_), myDriver(helper_)
        {

            overlap = 1;
            verbosity = true;

            if(helper.size() > 1)
            {
                solverParameters.solver = "CG";
                solverParameters.preconditioner = "OneLevel_AdditiveSchwarz";
                solverParameters.subdomainSolver = "UMFPack";
            }
            else
                solverParameters.solver = "UMFPack";

            q = 1e-2; //pressure of 50 Atmospheres in MPa
        }

    inline void GridTransformation(FieldVec & x, FieldVec & y)
    {
        double sech1 = 1.0 / std::cosh(lambda1 * (x[0] - xstar));
        double sech2 = 1.0 / std::cosh(lambda2 * (x[1] - ystar));
        y[2] = A * sech1 * sech1 * sech2 * sech2 * std::cos(lambda3 * (x[0] - xstar + delta));
    }

    inline void generateWrinkle()
    {
        A = 1.0;

        lambda1 = 1.0;
        lambda2 = 1.0;
        lambda3 = 1.0;

        xstar = 0.0;
        ystar = 0.0;

        delta = 0.0;
    }

    inline void apply()
    {
        myDriver.apply(*this);
    }


    inline bool isDirichlet(FieldVec & x, const int dof)
    {
        //! Default isDirichlet function, returning homogeneous Dirichlet boundary conditioners
        return (x[0] < 1e-6);
    }

    inline void evaluateWeight(FieldVec& f, int id) const
    {
        f = 0.0;
    }

    inline void evaluateNeumann(const FieldVec &x,FieldVec& h,const  FieldVec& normal) const
    {
        h = 0.0; // initialise to zero
        double T = 2.98; // Thickness
        if (x[2] > T - 1e-6){ // If element on top face of laminate, apply neumann boundary condition
            h[2] += q;
        }
    }

    //! \fn A member taking which constructs the base layered laminate in untransformed coordinates
    inline void LayerCake()
    {
        std::string example01a_Geometry = "stackingSequences/example1a.csv";
        LayerCakeFromFile(example01a_Geometry);
        GeometryBuilder();
    }

    template<class GO, class U, class GFS, class C,class MBE, class GV>
    inline void postprocess(const GO& go, U& u, const GFS& gfs, const C& cg, const GV gv, MBE& mbe)
    {
        using Dune::PDELab::Backend::native;
        double local_u3_max = 0.0;
        for (unsigned int i = 0; i < native(u).size(); i++){ // Loop over each vertex
            double u3 = std::abs(native(u)[i][2]);
            if (local_u3_max < u3){ local_u3_max = u3; }
        }
        MPI_Allreduce(&local_u3_max, &Q, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    }
};
