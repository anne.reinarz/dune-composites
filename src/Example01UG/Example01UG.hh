#include<dune/composites/Setup/baseGridModel.hh>
#include <dune/composites/Driver/linearStaticDriverUG.hh>
#include <dune/composites/PostProcessing/FailureCriteria/camanhoCriterion.hh>

#if HAVE_UG
#include <dune/grid/uggrid/uggridfactory.hh>

template<int DIM>
class Example01UG : public Dune::Composites::baseGridModel<Dune::UGGrid<3>>{

    public:
        double Q;
        int overlap;
        bool verbosity;

        Dune::Composites::linearStaticDriverUG<Example01UG<DIM>> myDriver;

        Example01UG(Dune::MPIHelper& helper_) : baseGridModel(helper_), myDriver(helper_){

            overlap = 1;
            verbosity = (helper.rank() == 0);
            solverParameters.KrylovTol = 1e-4;
            solverParameters.UG = "true";

            UG_input = "UGgrids/layercake_hex.msh";

            vtk_displacement_output = "Example01UG_displacement";
            vtk_stress_output = "Example01UG_stress";
            vtk_properties_output = "Example01UG_properties";

        };

    void inline apply(){
        myDriver.apply(*this);
    }

    void setUpMaterials(){
        numMaterials = 2;
        myMaterials.resize(numMaterials);

        // Material I: Isotropic Resin
        myMaterials[0].setType(0); // Isotropic
        myMaterials[0].setProp(0,8800.); // - E,  Young's Modulus
        myMaterials[0].setProp(1,0.314); // - nu,   Poisson Ratio
        myMaterials[0].setDensity(1.57e-5); // - rho,  Density  kg/mm^2

        // Material II: Orthotropic, Composite Ply - AS4/8552
        myMaterials[1].setType(1); // Orthotropic Composite
        myMaterials[1].setProp(0,137100.); // E11
        myMaterials[1].setProp(1,8800.); // E22
        myMaterials[1].setProp(2,8800.); // E33
        myMaterials[1].setProp(3,0.314); // nu12
        myMaterials[1].setProp(4,0.314); // nu13
        myMaterials[1].setProp(5,0.487); // nu23
        myMaterials[1].setProp(6,4900.); // G12
        myMaterials[1].setProp(7,4900.); // G13
        myMaterials[1].setProp(8,2959.); // G23
        myMaterials[1].setDensity(1.57e-5); // - rho, Density kg/mm^2
    }

    //Define how PG correspond to materials
        void assignMaterials(std::vector<int>& PGin_){
            elemIndx2PG.resize(PGin_.size());  //material number following the numbering above
            rot123.resize(PGin_.size());       //orientation of the material
            for(std::size_t id = 0; id < PGin_.size(); id++){
                int pg = PGin_[id];
                if(pg%2 == 0){
                    rot123[id] = 0;
                    elemIndx2PG[id] = 1;
                }
                if(pg%2 == 1){ //resin
                    rot123[id] = 0;
                    elemIndx2PG[id] = 0;
                }
            } // End for-loop of each element */
        }


    bool inline isDirichlet(FieldVec& x, const int dof){
        //! Default isDirichlet function, returning homogeneous Dirichlet boundary conditions
        if(x[0] > 40.0 -1e-3 && dof == 0)
            return true;
        return (x[0] < 1e-6);
    }

    double inline evaluateDirichlet(const FieldVec& x, int dof) const{
                    // Return homogeneous boundary conditions
        if(x[0] > 40.0 -1e-3 && dof == 0)
            return 0.277265;
        return 0.0;
    }


    inline void evaluateWeight(FieldVec& f, int id) const{
        //! body force, e.g. gravity
        f = 0.0;
    }

    double inline FailureCriteria(Dune::FieldVector<double,6>& stress) const{
        const std::vector<double> param = {61, 97, 94}; //material allowables in MPa
        std::vector<double> stress_std(6);
        for(int i= 0; i < 6; i++) stress_std[i] = stress[i];
        //TODO material type check
        return Dune::Composites::Camanho(stress_std, 0, param);
    }

    template<class GO, class U, class GFS, class C,class MBE, class GV>
    void inline postprocess(const GO& go, U& u, const GFS& gfs, const C& cg, const GV gv, MBE& mbe){
    }

};

#endif
