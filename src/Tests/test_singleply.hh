#include<dune/composites/Setup/baseGridModel.hh>
#include <dune/composites/Driver/linearStaticDriver.hh>

template<int DIM>
class Example01d : public Dune::Composites::baseGridModel< Dune::YaspGrid<3,Dune::TensorProductCoordinates<double,3>>>{

    public:

        double Q;
        int overlap;
        bool verbosity;

        Dune::Composites::linearStaticDriver<Example01d<DIM>> myDriver;

        Example01d(Dune::MPIHelper& helper_) : baseGridModel(helper_), myDriver(helper_){

            overlap = 1;
            verbosity = (helper.rank() == 0);

            if(helper.size() > 1){
                solverParameters.solver = "CG";
                solverParameters.preconditioner = "OneLevel_AdditiveSchwarz";
                solverParameters.subdomainSolver = "UMFPack";
            }
            else{
                solverParameters.solver = "UMFPack";
            }

            q = 1e-2; //pressure of 50 Atmospheres in MPa

            vtk_displacement_output = "Test_singleply_displacement";
            vtk_stress_output = "Test_singleply_stress";
            vtk_properties_output = "Test_singleply_properties"; 

            stress_Plot_ascii = true;

        };

    void inline apply(){
        myDriver.apply(*this);
    }


    bool inline isDirichlet(FieldVec& x, const int dof){
        //! Default isDirichlet function, returning homogeneous Dirichlet boundary conditions
        return (x[0] < 1e-6);
    }

    inline void evaluateWeight(FieldVec& f, int id) const{
        //! body force, e.g. gravity
        f = 0.0;
    }

    inline void evaluateNeumann(const FieldVec&x, FieldVec& h,const FieldVec& normal) const{
        //! neumann boundary
        h = 0.0; // initialise to zero
        double T = R[0].L[2]; // Thickness
        if (x[2] > T - 1e-6){ // If element on top face of laminate, apply neumann boundary condition
            h[2] += q;
        }
    }

    //! \fn A member taking which constructs the base layered laminate in untransformed coordinates
    void inline LayerCake(){
        std::string example01a_Geometry = "stackingSequences/example1d.csv";
        LayerCakeFromFile(example01a_Geometry);
        GeometryBuilder();
    }

    template<class GO, class U, class GFS, class C,class MBE, class GV>
    void inline postprocess(const GO& go, U& u, const GFS& gfs, const C& cg, const GV gv, MBE& mbe){
        using Dune::PDELab::Backend::native;
        double local_u3_max = 0.0;
        for (unsigned int i = 0; i < native(u).size(); i++){ // Loop over each vertex
            double u3 = std::abs(native(u)[i][2]);
            if (local_u3_max < u3){ local_u3_max = u3; }
        }
        MPI_Allreduce(&local_u3_max, &Q, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    }

};
