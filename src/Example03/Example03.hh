#include<dune/composites/Setup/baseGridModel.hh>
#include <dune/composites/Driver/linearStaticDriver.hh>
#include <dune/composites/PostProcessing/FailureCriteria/camanhoCriterion.hh>
#include <dune/composites/Setup/RandomFields/defectGenerator.hh>

template<int DIM>
class Example03 : public Dune::Composites::baseGridModel< Dune::YaspGrid<3,Dune::TensorProductCoordinates<double,3>>>{

    public:

        double Q;

        int overlap;
        bool verbosity;

        Dune::Composites::COEFF myDefect;
        Dune::Composites::linearStaticDriver<Example03<DIM>> myDriver;

        Example03(Dune::MPIHelper& helper_) : baseGridModel(helper_), myDriver(helper_){
            overlap = 1;
            verbosity = false;
            if(helper.rank() == 0) verbosity = true;

            solverParameters.solver = "GMRES";
            solverParameters.preconditioner = "GenEO";
            solverParameters.subdomainSolver = "UMFPack";
            solverParameters.overlap = overlap;
            solverParameters.coarseSpaceActive = true;
            solverParameters.nev = 35;
            solverParameters.nev_arpack = 35;
            solverParameters.verb = 0;
            if(helper.rank() == 0) solverParameters.verb = 10;
            solverParameters.eigenvalue_threshold = 1;
            solverParameters.widlund_part_unity = true;
            solverParameters.eigen_shift = 0.001;

            residualHeat = true;
            gravity = 0;

            q = 0; //pressure of 50 Atmospheres in MPa
        };

    void inline apply(){
        myDriver.apply(*this); 
    }

    std::array<int, 3> inline GridPartition(){
        // Function returns default partition
        std::array<int, 3> Partitioning;
        Partitioning[0] = 8;//config.get<int>("ProcX");
        Partitioning[1] = 1;//config.get<int>("ProcY");
        Partitioning[2] = 1;
        return Partitioning;
    }

    bool inline isDirichlet(FieldVec & x, const int dof){
        // Default isDirichlet function, returning homogeneous Dirichlet boundary conditioners
        return (x[1] < 1e-6);
    }

    inline void evaluateWeight(FieldVec& f, int id) const{
        f = 0.0;
    }

    inline void evaluateNeumann(const FieldVec &x,FieldVec& h,const  FieldVec& normal) const{
        h = 0.0; // initialise to zero
        double T = R[0].L[2]; // Thickness
        double radius = R[3].L[0]; //inner radius
        double limb_a = R[0].L[0] + R[1].L[0] + R[2].L[0];   //limb
        double limb_b = R[4].L[0] + R[5].L[0] + R[6].L[0];   //limb
        double p = 0.109; //fuel pressure

        //BC for wingbox example
        //1. Pressure bending down one end of the wingbox
        if(x[2] > (T + radius)*2 + limb_b -1e-06){
            for(int i = 0; i < 3; i++)
                h[i] = q * normal[i];
        }
        //2. Fuel pressure from the inside
        if( (x[0] > radius + T-1e-6 && x[0] < radius + T + limb_a -1e-6 && fabs(x[2] - T)<1e-6)
                || (x[0] > radius + T - 1e-6 && x[0] < radius + T + limb_a -1e-6 && fabs(x[2] - (T + limb_b + radius*2)) < 1e-6)
                || (x[2] > radius + T - 1e-6 && x[2] < radius + T + limb_b -1e-6 && fabs(x[0] - T) < 1e-6)
                || (x[2] > radius + T - 1e-6 && x[2] < radius + T + limb_b -1e-6 && fabs(x[0]-(T + limb_a + radius*2))<1e-6)
          ){
            for(int i = 0; i < 3; i++)
                h[i] = p * normal[i];
        }
    }

    double inline FailureCriteria(Dune::FieldVector<double,6>& stress) const{
        const std::vector<double> param = {61, 97, 94}; //material allowables in MPa
        std::vector<double> stress_std(6);
        for(int i= 0; i < 6; i++) stress_std[i] = stress[i];
        return Dune::Composites::Camanho(stress_std, 0, param);
    }

    bool inline isMPC(FieldVec& x){
        double length = R[0].L[1];
        return (x[1] > length - 1e-6);
    }

    FieldVec inline gridTransformation(const FieldVec & x) const{ 
        double radius = R[3].L[0]; //inner radius
        double limb_a = R[0].L[0] + R[1].L[0] + R[2].L[0];   //limb
        double limb_b = R[4].L[0] + R[5].L[0] + R[6].L[0];   //limb
        double thickness = R[0].L[2];    //thickness

        FieldVec ydef = defect(x);
        FieldVec y = ydef;

        double theta;
        //no transformation needed for first limb section

        //transform first radial section
        if(x[0] >= limb_a && x[0] <= limb_a + radius){
            //Angle in radians
            theta = M_PI/2*(ydef[0] - limb_a)/radius;
            //Radius
            double r = radius + thickness - ydef[2];
            y[0] = limb_a + r*sin(theta);
            y[1] = ydef[1];
            y[2] = radius + thickness - r*cos(theta);
        }
        //Model the second limb
        if(x[0] > limb_a + radius && x[0] <= limb_a + limb_b + radius){
            theta = M_PI/2;
            double y0 = ydef[0] - limb_a - radius;
            double y2 = ydef[2];
            y[0] = limb_a + radius + thickness - y2;
            y[1] = ydef[1];
            y[2] = radius + thickness + y0;
        }
        //model second radial section
        if(x[0] >= limb_a + limb_b + radius && x[0] <= limb_a + limb_b+2 * radius){
            //Angle in radians
            theta = M_PI/2*(ydef[0] - limb_a - limb_b - radius)/radius + M_PI/2;
            //Radius
            double r = radius + thickness - ydef[2];
            y[0] = limb_a + r*sin(theta);
            y[1] = ydef[1];
            y[2] = radius + thickness + limb_b - r*cos(theta);
        }
        //Model the third limb
        if(x[0] > limb_a + limb_b + 2*radius && x[0]<=2*limb_a + limb_b + 2*radius){
            theta = M_PI;
            double y0 = ydef[0] - limb_a -limb_b - 2*radius;
            double y2 = ydef[2];
            y[0] = limb_a + cos(theta)*y0 - sin(theta)*y2;
            y[1] = ydef[1];
            y[2] = 2*radius + 2*thickness +limb_b + sin(theta)*y0 + cos(theta)*y2;
        }
        //Model the third radial section
        if( x[0] > 2*limb_a + limb_b + 2*radius && x[0] <= 2*limb_a + limb_b + 3*radius){
            //Angle in radians
            theta = M_PI/2*(ydef[0]-2*limb_a-limb_b-2*radius)/radius + M_PI;
            //Radius
            double r = radius + thickness - ydef[2];
            y[0] = r*sin(theta);
            y[1] = ydef[1];
            y[2] = radius + thickness + limb_b - r*cos(theta);
        }
        //Model the fourth limb
        if(x[0] > 2*limb_a + limb_b + 3*radius && x[0]<=2*limb_a + 2*limb_b + 3*radius){
            theta = 3*M_PI/2;
            double y0 = ydef[0] - 2*limb_a - limb_b - 3*radius;
            double y2 = ydef[2];
            y[0] = -radius - thickness + cos(theta)*y0 - sin(theta)*y2;
            y[1] = ydef[1];
            y[2] = radius + thickness + limb_b + sin(theta)*y0 + cos(theta)*y2;
        }
        //Model final radial section
        if( x[0] > 2*limb_a + 2*limb_b + 3*radius ){
            //Angle in radians
            theta = M_PI/2*(ydef[0]-2*limb_a-2*limb_b-3*radius)/radius + 3*M_PI/2;
            //Radius
            double r = radius + thickness - ydef[2];
            y[0] = r*sin(theta);
            y[1] = ydef[1];
            y[2] = radius + thickness - r*cos(theta);
        }

        return y;
    }

    FieldVec inline defect(const FieldVec& x) const {
        double thickness = R[0].L[2]; // Thickness
        double radius = R[3].L[0]; //inner radius
        double limb_a = R[0].L[0] + R[1].L[0] + R[2].L[0];   //limb
        //double limb_b = R[4].L[0] + R[5].L[0] + R[6].L[0];   //limb
        double Y = R[0].L[1];

        double LengthX = 346;
        Dune::FieldVector<double,DIM> defect_location = {limb_a + radius/2.0, Y/2.0 , 1.25};
        double defect_size = 2;

        FieldVec y = x;

        //Define defect function
        if(x[0] > limb_a && x[0] < limb_a + radius){   //Place defect only in the curved section
            // In curvy bit
            double xval = (x[0] - limb_a)/radius * LengthX; // xval goes from 0 to 346

            double pix = 5.0/58.0;
            xval = xval*pix; // xval converted to mm

            double nexp = 4.0; // exponential for decay function in x. Must always be even.

            auto xd = std::log(1.0/98); // because the processed image is 98 pixels tall
            auto xdamp = std::exp(pow((x[0] - defect_location[0]),nexp) / (pow((radius/2.0),nexp) / xd ));

            auto zd = std::log(1.0/98.0);
            auto zcentre = thickness - defect_location[2]; // roughly the depth of the peak in the z-direction i.e. centre of the focussed part of scan
            double zdamp; //asymmetrical decay function to ensure 0 deflection at tool face

            if(x[2] < zcentre)
                zdamp = std::exp(pow((x[2] - zcentre),2) / (pow(zcentre,2) / zd)); // this one is different because the wrinkle is located at the inner radius.
            else
                zdamp = std::exp(pow((x[2] - zcentre),2) / (pow((thickness - zcentre),2) / zd)); // this one is different because the wrinkle is located at the inner radius.

            auto ydamp = std::exp(-5.0 * pow((x[1] - defect_location[1]),2) / (pow((Y/2.0),2) ));

            for (int i = 0; i < myDefect.getN(); i++){
                y[2] += defect_size * myDefect.evalPhi(xval,i) * myDefect.getxi(i) * xdamp * ydamp * zdamp;
            }
        }
        return y;
    }

    //! \fn A member taking which constructs the base layered laminate in untransformed coordinates
    void inline LayerCake(){
        //stacking sequence from file, geometry define by Geometry transformation
        std::string example02 = "stackingSequences/"+config.get<std::string>("stackingSequence");
        LayerCakeFromFile(example02);
        GeometryBuilder();
        periodic[0] = 1;
        //Define geometry transformation from cube to actual geometry
        myDefect.user_random_field();
    }

    template<class GO, class U, class GFS, class C,class MBE, class GV>
    void inline postprocess(const GO& go, U& u, const GFS& gfs, const C& cg, const GV gv, MBE& mbe){
        using Dune::PDELab::Backend::native;
        double local_u3_max = 0.0;
        for (unsigned int i = 0; i < native(u).size(); i++){ // Loop over each vertex
            double u3 = std::abs(native(u)[i][2]);
            if (local_u3_max < u3){ local_u3_max = u3; }
        }
        MPI_Allreduce(&local_u3_max, &Q, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    }

};
