// -*- tab-width: 4; indent-tabs-mode: nil -*-
// vi: set et ts=4 sw=4 sts=4:
#include <dune/composites/Setup/baseGridModel.hh>
#include <dune/composites/Driver/linearStaticDriver.hh>
#include <dune/composites/PostProcessing/FailureCriteria/camanhoCriterion.hh>

template<int DIM>
class Example02 : public Dune::Composites::baseGridModel< Dune::YaspGrid<3,Dune::TensorProductCoordinates<double,3>>>{

    public:

        double Q;

        int overlap;
        bool verbosity;

        Dune::Composites::linearStaticDriver<Example02<DIM>> myDriver;

        Example02(Dune::MPIHelper& helper_) : baseGridModel(helper_), myDriver(helper_)
        {

            overlap = 1;
            verbosity = false;
            verbosity = (helper.rank() == 0);
            solverParameters.solver = "Hypre";

            q = 1e-2; //pressure of 50 Atmospheres in MPa
        }

    inline void apply()
    {
        myDriver.apply(*this);
    }

    inline bool isDirichlet(Dune::FieldVector<double,3> & x, const int dof)
    {
        // Default isDirichlet function, returning homogeneous Dirichlet boundary conditioners
        return (x[0] < 1e-6);
    }

    inline void evaluateWeight(Dune::FieldVector<double,3>& f, int id) const
    {
        f = 0.0;
    }

    inline void evaluateNeumann(const Dune::FieldVector<double,3> &x,Dune::FieldVector<double,3>& h,const  Dune::FieldVector<double,3>& normal) const
    {
        h = 0.0; // initialise to zero
        double T = R[0].L[2]; // Thickness
        double X = R[0].L[0] + R[1].L[0];
        double Y = R[1].L[0];

        if( std::abs(x[0] - X) < 1e-6 && x[2] > T + Y){ // If element on top face of laminate, apply neumann boundary condition
            for(int i=0; i<DIM; i++) h[i] =  q * normal[i];
        }
    }

    inline double FailureCriteria(Dune::FieldVector<double,6>& stress) const
    {
        const std::vector<double> param = {61, 97, 94}; //material allowables in MPa
        std::vector<double> stress_std(6);
        for(int i= 0; i < 6; i++) stress_std[i] = stress[i];
        //TODO material type check
        return Dune::Composites::Camanho(stress_std, 0, param);
    }

    inline FieldVec gridTransformation(const FieldVec & x) const
    {
        double radius = R[1].L[0]; //inner radius
        assert(R[0].L[0] == R[2].L[0]);
        double limb = R[0].L[0];   //limb
        double T = R[0].L[2];    //thickness

        double angle = 90;

        FieldVec ydef = defect(x);
        FieldVec y = ydef;

        //Model the curve
        if(x[0] >= limb && x[0] <= limb + radius){
            //Angle in radians
            double theta = angle*M_PI/180*(ydef[0] - limb)/radius;
            //Radius
            double r = radius + T - ydef[2];
            y[0] = limb + r*sin(theta);
            y[1] = ydef[1];
            y[2] = radius + T - r*cos(theta);
        }
        //Model the second limb
        if(x[0] > limb + radius){
            double theta = angle*M_PI/180;
            double y0 = ydef[0] - limb - radius;
            double y2 = ydef[2];
            y[0] = limb + radius + T + cos(theta)*y0 - sin(theta)*y2;
            y[1] = ydef[1];
            y[2] = radius + T + sin(theta)*y0 + cos(theta)*y2;
        }
        return y;
    }

    inline FieldVec defect(const FieldVec& x) const
    {
        double radius = R[1].L[0]; //inner radius
        assert(R[0].L[0] == R[2].L[0]);
        double limb = R[0].L[0];   //limb
        double T = R[0].L[2];    //thickness

        double defect_size = 0.2;

        FieldVec y = x;

        Dune::FieldVector<double,3> damping = {2,.5,10};
        Dune::FieldVector<double,3> defect_location = {radius/2. + limb, R[0].L[1]/2., T/2};

        //Define defect function
        if(x[0] > limb && x[0] < limb + radius){   //Place defect only in the curved section
            y[2] += defect_size * cos(15*M_PI * (x[0] - limb)/radius)
                * 1./pow(cosh(damping[0] * M_PI * (defect_location[0] - x[0])/radius),2.)
                * 1./pow(cosh(damping[1] * M_PI * (defect_location[1] - x[1])/R[0].L[1]),2.)
                * 1./pow(cosh(damping[2] * M_PI * (defect_location[2] - x[2])/T),2.);
        }
        return y;
    }

    //! \fn A member taking which constructs the base layered laminate in untransformed coordinates
    inline void LayerCake()
    {
        //stacking sequence from file, geometry define by Geometry transformation
        std::string example02 = "stackingSequences/example2.csv";
        LayerCakeFromFile(example02);
        GeometryBuilder();
    }

    template<class GO, class U, class GFS, class C,class MBE, class GV>
    inline void postprocess(const GO& go, U& u, const GFS& gfs, const C& cg, const GV gv, MBE& mbe)
    {
        using Dune::PDELab::Backend::native;
        double local_u3_max = 0.0;
        for (unsigned int i = 0; i < native(u).size(); i++){ // Loop over each vertex
            double u3 = std::abs(native(u)[i][2]);
            if (local_u3_max < u3){ local_u3_max = u3; }
        }
        MPI_Allreduce(&local_u3_max, &Q, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    }

};
