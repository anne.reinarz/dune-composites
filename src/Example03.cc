#include <dune/composites/composites.hh>

#include "Example03/Example03.hh"



//===============================================================
// Main program with grid initialiseSolve
//===============================================================

int main(int argc, char** argv){

    //Read ini file
    Dune::ParameterTreeParser parser;
    parser.readINITree("configExample03.ini",config);
    //read in further options from command line
    parser.readOptions(argc,argv,config);

  const int DIM = 3;

  // Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    Example03<DIM> myModel(helper);

    myModel.apply();

    if(helper.rank() == 0) std::cout << "Solution of my Problem = " << myModel.Q << std::endl;
}
