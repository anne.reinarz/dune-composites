#ifndef _transpose_hh
#define _transpose_hh

namespace Dune{
    namespace Composites{

        template <typename X, int n>
            void transpose(const X& x, X& xt)
            {
                for (int i = 0; i < n; i++){
                    for (int j = 0; j < n; j++){
                        xt[i][j] = x[j][i];
                    }
                }
            }

        template <typename X, int m, int n>
            void transpose(const X& x, X& xt)
            {
                for (int i = 0; i < m; i++){
                    for (int j = 0; j < n; j++){
                        xt[i][j] = x[j][i];
                    }
                }
            }

    }
}

#endif
