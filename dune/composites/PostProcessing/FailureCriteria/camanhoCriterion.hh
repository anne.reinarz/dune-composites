#ifndef CAMANHO_CRITERION_HH
#define CAMANHO_CRITERION_HH

namespace Dune{
	namespace Composites{

		double Camanho(std::vector<double>& sig, int materialType, const std::vector<double>& param){
		    assert(param.size() == 3); // Check 3 parameters are supplied for Camanho
			assert(sig.size() == 6);
		    double s33 = param[0]; //in MPa
		    double s13 = param[1];
		    double s23 = param[2];
	
		    double maxF = sqrt(pow(std::max(sig[2],0.0)/s33,2) + pow(sig[4]/s13,2) + pow(sig[3]/s23,2));
		    if(materialType == 0){ //Only applicatble in resin layers
		        return maxF;
		    }
		    return 0.0;
		}
	}
}
#endif

