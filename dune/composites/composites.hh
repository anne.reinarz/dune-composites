#ifndef GENERAL_INCLUDES_HH
#define GENERAL_INCLUDES_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

//Parameter file and corresponding includes
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
// Marian: Global variables, especially of such kind, are considered bad style!
Dune::ParameterTree config;
Dune::ParameterTree configuration;

//General C includes
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <string>

//Dune includes
#include <dune/pdelab/gridfunctionspace/vectorgridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/subspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/grid/geometrygrid/grid.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/pdelab/ordering/chunkedblockordering.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

//UG grid includes
#if HAVE_UG
#include <dune/grid/uggrid/uggridfactory.hh>
#include <dune/grid/io/file/gmshreader.hh>
#endif

#endif
