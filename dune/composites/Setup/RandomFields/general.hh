// -*- tab-width: 4; indent-tabs-mode: nil -*-
// vi: set et ts=4 sw=4 sts=4:
#ifndef general_h
#define general_h

namespace Dune{
    namespace Composites{


        double mean(std::vector<double>& Y)
        {
            /*! Calculate mean*/
            double Yhat = 0.0;
            for (unsigned int i = 0; i < Y.size(); i++)
            {
                Yhat += Y[i];
            }
            Yhat /= Y.size();
            return Yhat;
        }

        double var(std::vector<double>& Y)
        {
            /*! Calculate variance*/
            double Yhat = mean(Y);
            double variance = 0.0;
            int N = Y.size();

            for (int i = 0; i < N; i++)
            {
                variance += (Y[i] - Yhat) * (Y[i] - Yhat);
            }
            variance /= (N - 1);
            return variance;
        }

    }
}


#endif /* general_h */
