#include <array>

#ifndef PARALLEL_PARTITION__HH
#define PARALLEL_PARTITION__HH

namespace Dune{
    namespace Composites{

        //! Partition yaspgrid for parallelism
        template<int d>
            class YaspPartition : public Dune::Yasp::Partitioning<d>
        {
            public:
                using iTuple = std::array<int,d>;
                void partition(const iTuple& size, int numProc, iTuple& dims, int overlap) const override
                {
                    dims = {1,1,1};
                    if(dims[0]*dims[1]*dims[2]!=numProc){

                        if(int(sqrt(numProc))*int(sqrt(numProc)) == numProc){
                            dims[0] = sqrt(numProc);
                            dims[1] = sqrt(numProc);
                        }
                        else{
                            if(int(numProc/8)*8 == numProc){
                                dims[0] = 8;
                                dims[1] = int(numProc/8);
                                dims[2] = 1;
                            }
                            else{
                                dims[0] = numProc;
                            }
                        }
                    }
                }
        };

    }
}

#endif
