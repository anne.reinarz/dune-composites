#ifndef LAYER__HH
#define LAYER__HH

namespace Dune{
    namespace Composites{

        /*! \brief Layer class
         *
         * Using the Layer class the Model::LayerCake() class can be extended to also define layers properties of each layer,
         * including thickness, material type, or orientation.
         */
        class Layer{

            public:

                Layer(){};
                void inline setElements(int nel_){ nel = nel_; }
                void inline setMaterial(int material_){ material = material_;}
                void inline setThickness(double thickness_){ thickness = thickness_;}
                void inline setOrientation(double theta_){ theta = theta_; }
                int inline getElements(){ return nel; }
                double inline getThickness(){ return thickness; }
                int inline getMaterial(){ return material; }
                double inline getOrientation(){return theta; }

            private:

                double thickness;
                int id;
                int material;
                int nel;
                double theta;
        };

    }
}


#endif
