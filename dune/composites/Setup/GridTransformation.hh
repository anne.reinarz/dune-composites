#ifndef DUNE_COMPOSITIES_GRIDTRANSFORMATION__HH
#define DUNE_COMPOSITIES_GRIDTRANSFORMATION__HH


namespace Dune{
  namespace Composites{

    //! Grid Transformation
    /*!
     * Gridtransformation is a user-defined function which
     * gives the transformations of points x in the base grid (generated using LayerCake()) to the final
     * structural geometry
     * \f$ y = F(x) : \Omega -> \hat \Omega. \f$
     * This function not only allows us to define more complex
     * geometry, but grade the mesh towards areas of interest and introduce geometric defects.
     */
    template <int dim, class MODEL>
    class GridTransformation
    : public Dune :: AnalyticalCoordFunction< double, dim, dim, GridTransformation <dim,MODEL> >{
      typedef GridTransformation This;
      typedef Dune :: AnalyticalCoordFunction< double, dim, dim, This > Base;

    public:
      typedef typename Base :: DomainVector DomainVector;
      typedef typename Base :: RangeVector RangeVector;

      GridTransformation(MODEL& model_, int my_rank_) : 
          model(model_), giveOutput(1), my_rank(my_rank_){  }

      void evaluate ( const DomainVector &x, RangeVector &y ) const {
          y = model.gridTransformation(x); 
      }

    private:
      MODEL model;
      mutable int giveOutput;
      int my_rank;
    };

  } // end composites namespace
} // end dune namespace

#endif
