#include <dune/composites/Setup/parallelPartition.hh>
#include <dune/composites/Setup/GridTransformation.hh>
#include <dune/composites/Setup/dirichletBC.hh>
#include <dune/composites/Driver/FEM/Serendipity/serendipityfem.hh>

#include "../PostProcessing/computeStresses.hh"
#include "../PostProcessing/plot_properties.hh"
#include "localOperator/linearelasticity.hh"

namespace Dune{
    namespace Composites{

        //! Static driver including a thermal loading
        /*!
         * this driver iterates until the failure load for a given
         * thermal strain is reached
         */
        template <typename MODEL>
            class heatStaticDriver{

                public:
                    typedef Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none,1> Scalar_VectorBackend;
                    typedef Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed,3> VectorBackend;
                    typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
                    typedef double RF;

                    int elem_order;

                    Dune::MPIHelper& helper;

                    heatStaticDriver(Dune::MPIHelper & helper_) : helper(helper_){
                        elem_order = 2; // Default Value which Can be overwritten by setElementOrder
                    };

                    void inline setElementOrder(int val = 2){
                        elem_order = val;
                    }

                    void inline apply(MODEL& myModel){
                        if (myModel.setUp_Required == true){
                            myModel.LayerCake();
                            myModel.setUpMaterials();
                            myModel.computeElasticTensors();
                        }

                        Dune::Timer watch;
                        std::vector<double> times(3);

                        // === A: Setup YaspGrid
                        watch.reset();
                        typedef Dune::YaspGrid<3,Dune::TensorProductCoordinates<double,3>> YGRID;
                        YaspPartition<3> yp;
                        YGRID yaspgrid(myModel.coords,myModel.periodic,myModel.overlap,helper.getCommunicator(),&yp);
                        int refinements = myModel.refineBaseGrid;
                        if(refinements > 0) yaspgrid.globalRefine(refinements);
                        typedef YGRID::LeafGridView YGV;
                        const YGV ygv = yaspgrid.leafGridView();
                        int size =  yaspgrid.globalSize(0)*yaspgrid.globalSize(1)*yaspgrid.globalSize(2);

                        if(helper.rank() == 0){
                            std::cout << "Number of elements per processor: " << ygv.size(0) << std::endl;
                            std::cout << "Number of nodes per processor: "    << ygv.size(3) << std::endl;
                        }

                        myModel.setPG(ygv); // Loops over elements and assigns a physical group

                        // ==================================================================
                        //                         Transform Grid
                        // ==================================================================
                        typedef Dune::Composites::GridTransformation<3,MODEL> GRID_TRAFO;
                        GRID_TRAFO gTrafo(myModel,helper.rank());

                        typedef typename Dune::GeometryGrid<YGRID,GRID_TRAFO> GRID;
                        GRID grid(yaspgrid,gTrafo);
                        if(helper.rank() == 0)
                            std::cout << "Grid transformation complete" << std::endl;

                        //Define Grid view
                        typedef typename GRID::LeafGridView GV;
                        const GV gv = grid.leafGridView();

                        if(helper.rank() == 0)
                            std::cout << "Grid view set up" << std::endl;

                        times[1] = watch.elapsed();
                        watch.reset();

                        typedef Scalar_BC<GV,MODEL,RF> BC;
                        typedef Dune::PDELab::CompositeConstraintsParameters<BC,BC,BC> Constraints;

                        // ==================================================================
                        //                         Set up problem
                        // ==================================================================
                        myModel.template computeTensorsOnGrid<GV,YGV>(gv,ygv);

                        // Setup initial boundary conditions for each degree of freedom
                        typedef Scalar_BC<GV,MODEL,double> InitialDisp;
                        InitialDisp u1(gv, myModel,0), u2(gv, myModel,1), u3(gv, myModel,2);

                        // Wrap scalar boundary conditions in to vector
                        typedef Dune::PDELab::CompositeGridFunction<InitialDisp,InitialDisp,InitialDisp>	InitialSolution;
                        InitialSolution initial_solution(u1,u2,u3);

                        // Construct grid function spaces for each degree of freedom
                        typedef Dune::PDELab::OverlappingConformingDirichletConstraints CON;
                        CON con;
                        typedef Dune::PDELab::ConformingDirichletConstraints CON_EXT; //needed only for Geneo solver

                        times[2] = watch.elapsed();
                        watch.reset();

                        if(elem_order == 2){ // == Element Order
                            const int element_order = 2;
                            const int dofel = 3 * 20;
                            const int non_zeros = 81;

                            if(helper.rank() == 0)
                                std::cout << "Piecewise quadratic serendipity elements" << std::endl;

                            typedef Dune::PDELab::SerendipityLocalFiniteElementMap<GV,typename GV::Grid::ctype,double,element_order> FEM;
                            FEM fem(gv);

                            using SCALAR_VBE = Dune::PDELab::ISTL::VectorBackend<>;
                            typedef Dune::PDELab::GridFunctionSpace<GV, FEM, CON, SCALAR_VBE> SCALAR_GFS;
                            typedef Dune::PDELab::GridFunctionSpace<GV, FEM, CON_EXT, SCALAR_VBE> SCALAR_GFS_EXT;
                            SCALAR_GFS U1(gv,fem,con); U1.name("U1");  SCALAR_GFS_EXT U1_EXT(gv,fem);
                            SCALAR_GFS U2(gv,fem,con); U2.name("U2");  SCALAR_GFS_EXT U2_EXT(gv,fem);
                            SCALAR_GFS U3(gv,fem,con); U3.name("U3");  SCALAR_GFS_EXT U3_EXT(gv,fem);

                            // Note that Vectors are blocked by Dune::PDELab::EntityBlockedOrderingTag
                            const int block_size = 3;
                            using VBE = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed>;
                            using Ordering = Dune::PDELab::EntityBlockedOrderingTag;
                            typedef Dune::PDELab::PowerGridFunctionSpace <SCALAR_GFS,block_size,VBE,Ordering> GFS;
                            const GFS gfs(U1,U2,U3);
                            typedef Dune::PDELab::PowerGridFunctionSpace <SCALAR_GFS_EXT,block_size,VBE,Ordering> GFS_EXT;
                            const GFS_EXT gfs_ext(U1_EXT,U2_EXT,U3_EXT);

                            typedef typename GFS::template ConstraintsContainer<RF>::Type C;

                            // Make constraints map and initialize it from a function
                            C cg;
                            cg.clear();

                            BC U1_cc(gv,myModel,0), U2_cc(gv,myModel,1), U3_cc(gv,myModel,2);
                            Constraints constraints(U1_cc,U2_cc,U3_cc);
                            Dune::PDELab::constraints(constraints,gfs,cg);

                            MBE mbe(non_zeros); // Maximal number of nonzeroes per row

                            //Correction to mechanical load
                            double lambda = 1.0; //(1-therm_fail)/mech_fail;
                            // 3. Rerun with mechanical and thermal load and double check
                            // TODO should probably introduce a loop here in case this is still quite far off
                            double total_fail = 0;
                            int cnt; //count number of solves
                            while(std::abs(total_fail-1.0) > 1e-2){  //tolerance to which to determine failure load
                                if(gv.comm().rank() == 0) std::cout << "Lambda " << lambda << std::endl;
                                myModel.setThermal(true);
                                myModel.setPressure(lambda);

                                typedef Dune::PDELab::linearelasticity<GV, MODEL, dofel> LOP;
                                LOP lop(gv, myModel);

                                typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,C,C> GO;
                                GO go(gfs,cg,gfs,cg,lop,mbe);
                                typedef Dune::PDELab::GridOperator<GFS_EXT,GFS_EXT,LOP,MBE,RF,RF,RF,C,C> GO_EXT;

                                // === Make coefficent vector and initialize it from a function
                                typedef Dune::PDELab::Backend::Vector<GFS,double> V;
                                V u(gfs,0.0);
                                Dune::PDELab::interpolate(initial_solution,gfs,u);

                                // === Set non constrained dofs to zero
                                Dune::PDELab::set_shifted_dofs(cg,0.0,u);

                                // === Solve the linear system
                                myModel.template solve<GO,GO_EXT,V,GFS,GFS_EXT,C,Constraints,MBE,LOP>(go,u,gfs,gfs_ext,cg,constraints,mbe,lop);
                                cnt++;
                                //if(gv.comm().rank() == 0) std::cout << "Check solution does something! ||x||_2 = " <<  u.two_norm() << std::endl;

                                if(gv.comm().rank() == 0) std::cout << "=== Calculate Stresses" << std::endl;
                                calculateStresses<MODEL,V,GV,GFS,MBE>(myModel,u,gv,gfs,mbe);

                                // Pure thermal failure
                                using Dune::PDELab::Backend::native;
                                double max = 0.0;
                                typedef typename GV::Traits::template Codim<0>::Iterator ElementIterator;
                                for (ElementIterator it = gv.template begin<0>(); it!=gv.template end<0>(); ++it)
                                { // loop through each element
                                    int id = gv.indexSet().index(*it);
                                    auto stress = myModel.getStress(id);
                                    double h = myModel.FailureCriteria(stress);
                                    if(h > max) max = h;
                                }
                                MPI_Allreduce(&max, &total_fail, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
                                if(gv.comm().rank() == 0) std::cout << "Total failure " << total_fail << std::endl;

                                lambda = lambda/total_fail;

                                if(std::abs(total_fail-1.0)<1e-2){ //If done, plot and postprocess
                                    if(gv.comm().rank() == 0) std::cout << "Number of solves " << cnt << std::endl;

                                    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(0));
                                    Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,u);
                                    vtkwriter.write(myModel.vtk_displacement_output,Dune::VTK::appendedraw);

                                    plotProperties<MODEL,V,GV,GFS,MBE>(myModel,gv,gfs,mbe);

                                    myModel.template postprocess<GO,V,GFS,C,MBE,GV>(go,u,gfs,cg,gv,mbe);
                                }
                            }

                        }
                    } // end apply

            };
    }
}
