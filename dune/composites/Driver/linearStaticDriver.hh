// -*- tab-width: 4; indent-tabs-mode: nil -*-
// vi: set et ts=4 sw=4 sts=4:
#include <dune/composites/Setup/parallelPartition.hh>
#include <dune/composites/Setup/GridTransformation.hh>
#include <dune/composites/Setup/dirichletBC.hh>
#include <dune/composites/Driver/FEM/Serendipity/serendipityfem.hh>

#include "../PostProcessing/computeStresses.hh"
#include "../PostProcessing/plot_properties.hh"
#include "localOperator/linearelasticity.hh"

namespace Dune{
    namespace Composites{

        //! linear static driver
        template <typename MODEL>
            class linearStaticDriver{

                public:
                    typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
                    typedef double RF;

                    int elem_order;

                    Dune::MPIHelper& helper;

                    linearStaticDriver(Dune::MPIHelper & helper_) : helper(helper_){
                        elem_order = 2; // Default Value which Can be overwritten by setElementOrder
                    };

                    void inline setElementOrder(int val = 2){
                        elem_order = val;
                    }

                    void inline apply(MODEL& myModel){
                        if (myModel.setUp_Required == true){
                            myModel.LayerCake();
                            myModel.setUpMaterials();
                            myModel.computeElasticTensors();
                        }

                        Dune::Timer watch;
                        std::vector<double> times(3);

                        // === A: Setup YaspGrid
                        watch.reset();
                        typedef Dune::YaspGrid<3,Dune::TensorProductCoordinates<RF,3>> YGRID;
                        YaspPartition<3> yp;
                        YGRID yaspgrid(myModel.coords,myModel.periodic,myModel.overlap,helper.getCommunicator(),&yp);
                        int refinements = myModel.refineBaseGrid;
                        if(refinements > 0) yaspgrid.globalRefine(refinements);
                        typedef YGRID::LeafGridView YGV;
                        const YGV ygv = yaspgrid.leafGridView();

                        if(helper.rank() == 0){
                            std::cout << "Number of elements per processor: " << ygv.size(0) << std::endl;
                            std::cout << "Number of nodes per processor: "    << ygv.size(3) << std::endl;
                        }

                        myModel.setPG(ygv); // Loops over elements and assigns a physical group

                        // ==================================================================
                        //                         Transform Grid
                        // ==================================================================
                        typedef Dune::Composites::GridTransformation<3,MODEL> GRID_TRAFO;
                        GRID_TRAFO gTrafo(myModel, helper.rank());

                        typedef typename Dune::GeometryGrid<YGRID,GRID_TRAFO> GRID;
                        GRID grid(yaspgrid,gTrafo);
                        if(helper.rank() == 0)
                            std::cout << "Grid transformation complete" << std::endl;

                        //Define Grid view
                        typedef typename GRID::LeafGridView GV;
                        const GV gv = grid.leafGridView();

                        if(helper.rank() == 0)
                            std::cout << "Grid view set up" << std::endl;

                        times[1] = watch.elapsed();
                        watch.reset();

                        typedef Scalar_BC<GV,MODEL,RF> BC;
                        typedef Dune::PDELab::CompositeConstraintsParameters<BC,BC,BC> Constraints;

                        // ==================================================================
                        //                         Set up problem
                        // ==================================================================
                        myModel.template computeTensorsOnGrid<GV,YGV>(gv,ygv);

                        // Setup initial boundary conditions for each degree of freedom
                        typedef Scalar_BC<GV,MODEL,RF> InitialDisp;
                        InitialDisp u1(gv, myModel,0), u2(gv, myModel,1), u3(gv, myModel,2);

                        // Wrap scalar boundary conditions in to vector
                        typedef Dune::PDELab::CompositeGridFunction<InitialDisp,InitialDisp,InitialDisp>	InitialSolution;
                        InitialSolution initial_solution(u1,u2,u3);

                        // Construct grid function spaces for each degree of freedom
                        typedef Dune::PDELab::OverlappingConformingDirichletConstraints CON;
                        CON con;
                        typedef Dune::PDELab::ConformingDirichletConstraints CON_EXT; //needed only for Geneo solver

                        times[2] = watch.elapsed();
                        watch.reset();

                        if(elem_order == 2){ // == Element Order
                            const int element_order = 2;
                            const int dofel = 3 * 20;
                            const int non_zeros = 81;

                            if(helper.rank() == 0)
                                std::cout << "Piecewise quadratic serendipity elements" << std::endl;

                            typedef Dune::PDELab::SerendipityLocalFiniteElementMap<GV,typename GV::Grid::ctype,RF,element_order> FEM;
                            FEM fem(gv);

                            // Create AllEntitySet to be passed to ALL grid function spaces
                            typedef Dune::PDELab::AllEntitySet<GV> ES_ALL;
                            ES_ALL es_all(gv);

                            using SCALAR_VBE = Dune::PDELab::ISTL::VectorBackend<>;
                            typedef Dune::PDELab::GridFunctionSpace<ES_ALL, FEM, CON, SCALAR_VBE> SCALAR_GFS;
                            typedef Dune::PDELab::GridFunctionSpace<ES_ALL, FEM, CON_EXT, SCALAR_VBE> SCALAR_GFS_EXT;
                            SCALAR_GFS U1(es_all,fem,con); U1.name("U1");  SCALAR_GFS_EXT U1_EXT(es_all,fem);
                            SCALAR_GFS U2(es_all,fem,con); U2.name("U2");  SCALAR_GFS_EXT U2_EXT(es_all,fem);
                            SCALAR_GFS U3(es_all,fem,con); U3.name("U3");  SCALAR_GFS_EXT U3_EXT(es_all,fem);

                            // Note that Vectors are blocked by Dune::PDELab::EntityBlockedOrderingTag
                            const int block_size = 3;
                            using VBE = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed>;
                            using Ordering = Dune::PDELab::EntityBlockedOrderingTag;
                            typedef Dune::PDELab::PowerGridFunctionSpace <SCALAR_GFS,block_size,VBE,Ordering> GFS;
                            const GFS gfs(U1,U2,U3);
                            typedef Dune::PDELab::PowerGridFunctionSpace <SCALAR_GFS_EXT,block_size,VBE,Ordering> GFS_EXT;
                            const GFS_EXT gfs_ext(U1_EXT,U2_EXT,U3_EXT);

                            typedef typename GFS::template ConstraintsContainer<RF>::Type C;

                            // Make constraints map and initialize it from a function
                            C cg;
                            cg.clear();

                            BC U1_cc(gv,myModel,0), U2_cc(gv,myModel,1), U3_cc(gv,myModel,2);
                            Constraints constraints(U1_cc,U2_cc,U3_cc);

                            Dune::PDELab::constraints(constraints,gfs,cg);

                            MBE mbe(non_zeros); // Maximal number of nonzeroes per row

                            // === Construct Linear Operator on FEM Space
                            typedef Dune::PDELab::linearelasticity<GV, MODEL, dofel> LOP;
                            LOP lop(gv, myModel);

                            typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,C,C> GO;
                            GO go(gfs,cg,gfs,cg,lop,mbe);
                            typedef Dune::PDELab::GridOperator<GFS_EXT,GFS_EXT,LOP,MBE,RF,RF,RF,C,C> GO_EXT;

                            // === Make coefficent vector and initialize it from a function
                            typedef Dune::PDELab::Backend::Vector<GFS,double> V;
                            V u(gfs,0.0);
                            Dune::PDELab::interpolate(initial_solution,gfs,u);

                            // === Set non constrained dofs to zero
                            Dune::PDELab::set_shifted_dofs(cg,0.0,u);
                            myModel.template solve<GO,GO_EXT,V,GFS,GFS_EXT,C,Constraints,MBE,LOP>(go,u,gfs,gfs_ext,cg,constraints,mbe,lop);

                            Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(0));
                            Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,u);
                            vtkwriter.write(myModel.vtk_displacement_output,Dune::VTK::appendedraw);

                            calculateStresses<MODEL,V,GV,GFS,MBE>(myModel,u,gv,gfs,mbe);

                            plotProperties<MODEL,V,GV,GFS,MBE>(myModel,gv,gfs,mbe);

                            myModel.template postprocess<GO,V,GFS,C,MBE,GV>(go,u,gfs,cg,gv,mbe);
                        }
                    } // end apply

            };
    }
}
