// -*- tab-width: 4; indent-tabs-mode: nil -*-
// vi: set et ts=4 sw=4 sts=4:
#include <dune/composites/Setup/parallelPartition.hh>
#include <dune/composites/Setup/GridTransformation.hh>
#include <dune/composites/Setup/dirichletBC.hh>
#include <dune/composites/Driver/FEM/Serendipity/serendipityfem.hh>

#include "../PostProcessing/computeStresses.hh"
#include "../PostProcessing/plot_properties.hh"
#include "localOperator/linearelasticity.hh"

#if HAVE_UG
#include <dune/grid/uggrid/uggridfactory.hh>

#if HAVE_PARMETIS
#include <parmetis.h>
#ifdef PARMETIS_MAJOR_VERSION
#include <dune/grid/utility/parmetisgridpartitioner.hh>
#endif
#endif

namespace Dune{
    namespace Composites{

        //! linear static driver
        template <typename MODEL>
            class linearStaticDriverUG{

                public:
                    typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
                    typedef double RF;

                    int elem_order;

                    Dune::MPIHelper& helper;

                    linearStaticDriverUG(Dune::MPIHelper & helper_) : helper(helper_){
                        elem_order = 2; // Default Value which Can be overwritten by setElementOrder
                    };

                    void inline setElementOrder(int val = 2){
                        elem_order = val;
                    }

                    void inline apply(MODEL& myModel){
                        if (myModel.setUp_Required == true){ //initialise space
                            myModel.LayerCake();
                            myModel.setUpMaterials();
                            myModel.computeElasticTensors();
                        }

                        Dune::Timer watch;
                        std::vector<double> times(3);

                        // === A: Setup UGGrid
                        watch.reset();
                        typedef Dune::UGGrid<3> GRID;
                        Dune::GridFactory<GRID> factory;
                        std::string grid_file = config.get<std::string>("mshName",myModel.UG_input);
                        std::vector<int> bndIndx2PG;   //TODO
                        std::vector<int> PGin;
                        // no need to read the file on a single rank by yourself
                        // since version 2.7 this is taken care of internally
                        Dune::GmshReader<GRID>::read(factory,grid_file,bndIndx2PG,PGin,true,false);
                        std::shared_ptr<GRID> grid(factory.createGrid());
#if not (HAVE_PARMETIS and defined(PARMETIS_MAJOR_VERSION))
                        grid->loadBalance();
#else
                        if(helper.size() > 1 and helper.rank() == 0)
                            std::cout << "Using the ParMETIS grid partitioner ..." << std::endl;
                        std::vector<unsigned> partitioner(Dune::ParMetisGridPartitioner<typename GRID::LeafGridView>::partition(grid->leafGridView(),helper));
                        grid->loadBalance(partitioner,0);
#endif // not (HAVE_PARMETIS and defined(PARMETIS_MAJOR_VERSION))
                        if(helper.size() > 1)
                        {
                            const auto& comm = helper.getCollectiveCommunication();
                            // resize vectors consistently on all ranks
                            std::size_t bndIndx2PG_size = bndIndx2PG.size();
                            std::size_t PGin_size = PGin.size();
                            comm.broadcast(&bndIndx2PG_size,1,0);
                            comm.broadcast(&PGin_size,1,0);
                            bndIndx2PG.resize(bndIndx2PG_size);
                            PGin.resize(PGin_size);
                            // broadcast data to all ranks after resize
                            comm.broadcast(&bndIndx2PG[0],bndIndx2PG_size,0);
                            comm.broadcast(&PGin[0],PGin_size,0);
                        }
                        myModel.assignMaterials(PGin);

                        typedef Dune::UGGrid<3>::LeafGridView GV;
                        const GV gv=grid->leafGridView();
                        std::cout << "Grid view set up" << std::endl;

                        myModel.template computeTensorsOnGrid<GV>(gv);

                        times[1] = watch.elapsed();
                        watch.reset();

                        typedef Scalar_BC<GV,MODEL,RF> BC;
                        typedef Dune::PDELab::CompositeConstraintsParameters<BC,BC,BC> Constraints;

                        // ==================================================================
                        //                         Set up problem
                        // ==================================================================

                        // Setup initial boundary conditions for each degree of freedom
                        typedef Scalar_BC<GV,MODEL,RF> InitialDisp;
                        InitialDisp u1(gv, myModel,0), u2(gv, myModel,1), u3(gv, myModel,2);

                        // Wrap scalar boundary conditions in to vector
                        typedef Dune::PDELab::CompositeGridFunction<InitialDisp,InitialDisp,InitialDisp>	InitialSolution;
                        InitialSolution initial_solution(u1,u2,u3);

                        // Construct grid function spaces for each degree of freedom
                        typedef Dune::PDELab::NonoverlappingConformingDirichletConstraints<GV> CON;
                        CON con(gv);

                        times[2] = watch.elapsed();
                        watch.reset();

                        if(gv.template begin<0>()->geometry().type().isCube()){
                            if(helper.rank() == 0)
                                std::cout << "UGGrid: Hexahedral elements" << std::endl;
                            // Marian: Shadowing class members is considered bad style!
                            const int element_order = 2;
                            const int dofel = 3 * 20;
                            const int non_zeros = 81;

                            if(helper.rank() == 0)
                                std::cout << "Piecewise quadratic serendipity elements" << std::endl;

                            typedef Dune::PDELab::SerendipityLocalFiniteElementMap<GV,typename GV::Grid::ctype,RF,element_order> FEM;
                            FEM fem(gv);

                            using SCALAR_VBE = Dune::PDELab::ISTL::VectorBackend<>;
                            typedef Dune::PDELab::GridFunctionSpace<GV, FEM, CON, SCALAR_VBE> SCALAR_GFS;
                            SCALAR_GFS U1(gv,fem,con); U1.name("U1");
                            SCALAR_GFS U2(gv,fem,con); U2.name("U2");
                            SCALAR_GFS U3(gv,fem,con); U3.name("U3");

                            // Note that Vectors are blocked by Dune::PDELab::EntityBlockedOrderingTag
                            const int block_size = 3;
                            using VBE = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed>;
                            using Ordering = Dune::PDELab::EntityBlockedOrderingTag;
                            typedef Dune::PDELab::PowerGridFunctionSpace <SCALAR_GFS,block_size,VBE,Ordering> GFS;
                            GFS gfs(U1,U2,U3);
                            con.compute_ghosts(gfs);

                            typedef typename GFS::template ConstraintsContainer<RF>::Type C;

                            // Make constraints map and initialize it from a function
                            C cg;
                            cg.clear();

                            BC U1_cc(gv,myModel,0), U2_cc(gv,myModel,1), U3_cc(gv,myModel,2);
                            Constraints constraints(U1_cc,U2_cc,U3_cc);

                            Dune::PDELab::constraints(constraints,gfs,cg);

                            MBE mbe(non_zeros); // Maximal number of nonzeroes per row

                            // === Construct Linear Operator on FEM Space
                            typedef Dune::PDELab::linearelasticity<GV, MODEL, dofel> LOP;
                            LOP lop(gv, myModel);

                            typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,C,C> GO;
                            GO go(gfs,cg,gfs,cg,lop,mbe);

                            // === Make coefficent vector and initialize it from a function
                            typedef Dune::PDELab::Backend::Vector<GFS,double> V;
                            V u(gfs,0.0);
                            Dune::PDELab::interpolate(initial_solution,gfs,u);

                            // === Set non constrained dofs to zero
                            Dune::PDELab::set_shifted_dofs(cg,0.0,u);
                            myModel.template solve<GO,GO,V,GFS,GFS,C,Constraints,MBE,LOP>(go,u,gfs,gfs,cg,constraints,mbe,lop);

                            Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(0));
                            Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,u);
                            vtkwriter.write(myModel.vtk_displacement_output,Dune::VTK::appendedraw);

                            plotProperties<MODEL,V,GV,GFS,MBE>(myModel,gv,gfs,mbe);

                            calculateStresses<MODEL,V,GV,GFS,MBE>(myModel,u,gv,gfs,mbe,dofel);

                            myModel.template postprocess<GO,V,GFS,C,MBE,GV>(go,u,gfs,cg,gv,mbe);
                        }
                        else{
                            if(helper.rank() == 0)
                                std::cout << "UGGrid: Piecewise linear elements" << std::endl;
                            const int element_order = 1; // Element order 1 - linear, 2 - quadratic
                            const int dofel = 12;
                            const int non_zeros = 27;
                            typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GV::Grid::ctype,double,element_order> FEM;         // Construct FEM Space         FEM fem(gv);
                            FEM fem(gv);

                            typedef Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none,1> Scalar_VectorBackend;

                            typedef Dune::PDELab::GridFunctionSpace<GV, FEM, CON, Scalar_VectorBackend> SCALAR_GFS;
                            SCALAR_GFS dispU1(gv,fem,con); dispU1.name("U1");
                            SCALAR_GFS dispU2(gv,fem,con); dispU2.name("U2");
                            SCALAR_GFS dispU3(gv,fem,con); dispU3.name("U3");
                            using VBE= Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed>;
                            using Ordering = Dune::PDELab::EntityBlockedOrderingTag;
                            typedef Dune::PDELab::PowerGridFunctionSpace <SCALAR_GFS, 3, VBE, Ordering> GFS;
                            const GFS gfs(dispU1, dispU2, dispU3);

                            con.compute_ghosts(gfs);

                            typedef typename GFS::template ConstraintsContainer<RF>::Type C;
                            // Make constraints map and initialize it from a function
                            C cg;
                            cg.clear();

                            BC U1_cc(gv,myModel,0), U2_cc(gv,myModel,1), U3_cc(gv,myModel,2);
                            Constraints constraints(U1_cc,U2_cc,U3_cc);

                            Dune::PDELab::constraints(constraints,gfs,cg);

                            MBE mbe(non_zeros); // Maximal number of nonzeroes per row

                            // === Construct Linear Operator on FEM Space
                            typedef Dune::PDELab::linearelasticity<GV, MODEL, dofel> LOP;
                            LOP lop(gv, myModel);

                            typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,C,C> GO;
                            GO go(gfs,cg,gfs,cg,lop,mbe);

                            // === Make coefficent vector and initialize it from a function
                            typedef Dune::PDELab::Backend::Vector<GFS,double> V;
                            V u(gfs,0.0);
                            Dune::PDELab::interpolate(initial_solution,gfs,u);

                            // === Set non constrained dofs to zero
                            Dune::PDELab::set_shifted_dofs(cg,0.0,u);
                            myModel.template solve<GO,GO,V,GFS,GFS,C,Constraints,MBE,LOP>(go,u,gfs,gfs,cg,constraints,mbe,lop);

                            Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,Dune::refinementLevels(0));
                            Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,u);
                            vtkwriter.write(myModel.vtk_displacement_output,Dune::VTK::appendedraw);

                            plotPropertiesUG<MODEL,V,GV,GFS,MBE>(myModel,gv,gfs,mbe);

                            calculateStressesUG<MODEL,V,GV,GFS,MBE>(myModel,u,gv,gfs,mbe,dofel);

                            myModel.template postprocess<GO,V,GFS,C,MBE,GV>(go,u,gfs,cg,gv,mbe);
                        }
                    } // end apply

            };
    }
}

#endif
