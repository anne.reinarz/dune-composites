// -*- tab-width: 4; c-basic-offset: 2; indent-tabs-mode: nil -*-

#ifndef DUNE_PDELAB_COORDINATEVECTOR_HH
#define DUNE_PDELAB_COORDINATEVECTOR_HH

namespace Dune {
    namespace PDELab {

template<typename CM>
class coordinateVector :
        public NumericalJacobianApplyVolume<coordinateVector<CM> >,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<double>,
        public NumericalJacobianVolume<coordinateVector<CM> >
    {
    public:
      // pattern assembly flags
      enum { doPatternVolume = false };

      // residual assembly flags
      enum { doLambdaVolume  = true };

      //Constructor
      coordinateVector (CM cm_): cm(cm_){}

      // alpha_volume for countElem
      template<typename EG, typename LFSV, typename R>
        void lambda_volume (const EG& eg, const LFSV& lfsv, R& r) const
        {
          //Unwrap function spaces
          const auto& lfsv1 = lfsv.template child<0>();
          const auto& lfsv2 = lfsv.template child<1>();
          const auto& lfsv3 = lfsv.template child<2>();
          //Accumulate coordinate values
          for (int i = 0; i < lfsv1.size()/3; i++){
              auto xh  = eg.geometry().local(eg.geometry().corner(i));
              Dune::FieldVector<double,1> numElem;
              cm.evaluate(eg.entity(),xh,numElem);
              r.accumulate(lfsv1, i, eg.geometry().corner(i)[0]/numElem[0]);
              r.accumulate(lfsv2, i, eg.geometry().corner(i)[1]/numElem[0]);
              r.accumulate(lfsv3, i, eg.geometry().corner(i)[2]/numElem[0]);
          }
        }
    private:
      CM cm;
    };

class countElem :
        public NumericalJacobianApplyVolume<countElem >,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<double>,
        public NumericalJacobianVolume<countElem >
    {
    public:
      // pattern assembly flags
      enum { doPatternVolume = false };

      // residual assembly flags
      enum { doLambdaVolume  = true };

      //Constructor
      countElem (){}
      
      // alpha_volume for countElem
      template<typename EG, typename LFSV, typename R>
        void lambda_volume (const EG& eg, const LFSV& lfsv, R& r) const
        { 
          for (int i = 0; i < lfsv.size(); i++){
            r.accumulate(lfsv, i, 1.);
          }
        }
    };
}
}
#endif
