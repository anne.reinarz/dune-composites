// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_PDELAB_FINITEELEMENTMAP_SERENDIPITYFEM_HH
#define DUNE_PDELAB_FINITEELEMENTMAP_SERENDIPITYFEM_HH

#include <cstddef>

#include "serendipity.hh"
#include <dune/pdelab/finiteelementmap/finiteelementmap.hh>

namespace Dune {
  namespace PDELab {

    //! wrap up element from local functions
    //! \ingroup FiniteElementMap
    template<typename GV, typename D, typename R, std::size_t k>
      class SerendipityLocalFiniteElementMap
      : public SimpleLocalFiniteElementMap< Dune::SerendipityLocalFiniteElement<D,R,GV::dimension,k>,GV::dimension>
      {

        public:

          static_assert(
              GV::dimension <= 3,
              "SerendipityLocalFiniteElementMap is only implemented for d = 1,2,3"
              );

          static_assert(
              k == 2,
              "SerendipityLocalFiniteElementMap is only available for k = 2"
              );

          SerendipityLocalFiniteElementMap(const GV& gv)
          {}

          static constexpr bool fixedSize()
          {
            return true;
          }

          static constexpr bool hasDOFs(int codim)
          {
            return false;
          }

          static constexpr std::size_t size(GeometryType gt)
          {
            return (gt.isVertex() || gt.isLine() ) ? 1 : 0;
          }

          static constexpr std::size_t maxLocalSize()
          {
            const auto d = GV::dimension;
            if(d == 1){
              return 3;
            }
            if(d == 2){
              return 12;
            }
            if(d == 3){
              return 20;
            }
          }

      };

  }
}

#endif // DUNE_PDELAB_FINITEELEMENTMAP_SERENDIPITYFEM_HH
